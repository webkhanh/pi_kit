#include "Shader.h"

#include <sstream>
#include <fstream>

namespace pi_kit {

static const char* s_default_vertex = R"(
attribute vec4 position;
uniform mat4 projection;

void main(void)
{
    gl_Position = projection * position;
}
)";

static const char* s_default_fragment = R"(
uniform vec4 color;

void main(void)
{
    gl_FragColor=vec4(color.rgba);
}
)";

//-------------------------------------------------------------------------------------------------

Shader::Shader()
{
    m_id = glCreateProgram();
    
    m_vertex_id = attach_shader(m_id, GL_VERTEX_SHADER, s_default_vertex);
    m_fragment_id = attach_shader(m_id, GL_FRAGMENT_SHADER, s_default_fragment);

    glLinkProgram(m_id);
    glUseProgram(m_id);

    if (glGetError() != 0)
    {
        throw std::runtime_error("Cannot initialize Shader");
    }
}

//-------------------------------------------------------------------------------------------------

Shader::~Shader()
{   
    detach_and_delete_shader(m_id, m_vertex_id);
    detach_and_delete_shader(m_id, m_fragment_id);
    glDeleteProgram(m_id);
}

//-------------------------------------------------------------------------------------------------

int Shader::attach_shader(const int program_id, const int type, const char* source)
{
    int id = glCreateShader(type);
    glShaderSource(id, 1, &source, 0);
    glCompileShader(id);
    glAttachShader(program_id, id);

    if (glGetError() != 0)
    {
        throw std::runtime_error("Cannot attach Shader");
    }

    return id;
}

//-------------------------------------------------------------------------------------------------

void Shader::detach_and_delete_shader(int program_id, int id)
{
    glDetachShader(program_id, id);
    glDeleteShader(id);    
}

//-------------------------------------------------------------------------------------------------
}//pi_kit
