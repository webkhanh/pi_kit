#include "Star.h"

namespace pi_kit{

//-------------------------------------------------------------------------------------------------

Star::Star(int num_points, float ratio)
: Shape()
, m_num_points(num_points)
{
    if (num_points <= 2)
    {
        throw std::runtime_error("Star must have at least 3 points");
    }

    m_vertices.reset(new float[(num_points *2 +2) *3]);

    m_vertices[0] = 0;
    m_vertices[1] = 0;
    m_vertices[2] = 0;

    float angle = (2.f * 3.141592f) / (num_points *2);
    for (int i = 0; i < num_points * 2 + 1; ++i)
    {
        v2 point(cos(angle * i), sin(angle * i));
        if (i%2 == 1)
        {
            point *= ratio;
        }

        m_vertices[(i+1) * 3] = point.x / 2.f;
        m_vertices[(i+1) * 3 +1] = point.y / 2.f;
        m_vertices[(i+1) * 3 +2] = 0;
    }
}

//-------------------------------------------------------------------------------------------------

void Star::render() const
{
    glDrawArrays(GL_TRIANGLE_FAN, 0, (m_num_points * 2) + 2);
}

//-------------------------------------------------------------------------------------------------

void Star::info()
{
    Log::title("Object", "Star");
    Log::info(description());
    Log::info("Is a Shape Object:");
    Shape::info();
}

//-------------------------------------------------------------------------------------------------
}//pi_kit
