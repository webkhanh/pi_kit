#pragma once

#include "Shape.h"
#include "GL_Simple2D.h"

namespace pi_kit{

//-------------------------------------------------------------------------------------------------

class Box: public Shape
{
    static const char* description()
    {
        return "Rectangular Shape";
    }

    void info() override;

public:

    Box();

    float* get_vertices() const override;
    void render() const override;

};

//-------------------------------------------------------------------------------------------------
}//pi_kit
