#pragma once

#include "Shape.h"
#include "GL_Simple2D.h"

namespace pi_kit{

//-------------------------------------------------------------------------------------------------

class Polygon: public Shape
{
    static const char* description()
    {
        return "Regular Polygon Shape";
    }

public:

    Polygon(int numSides);

    float* get_vertices() const override {return m_vertices.get();}
    void render() const override;

    void set_num_visible_sides(int num) {m_numVisibleSides = num;}

    void on_bind(Lua& lua, Lua::Object& object) override;
    void info() override;

private:

    int m_numSides;
    int m_numVisibleSides;
    std::unique_ptr<float[]> m_vertices;

};

//-------------------------------------------------------------------------------------------------
}//pi_kit
