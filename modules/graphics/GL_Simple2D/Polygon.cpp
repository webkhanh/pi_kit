#include "Polygon.h"

namespace pi_kit{

//-------------------------------------------------------------------------------------------------

Polygon::Polygon(int num_sides)
: Shape()
, m_numSides(num_sides)
, m_vertices(new float[(num_sides + 2)*3])
, m_numVisibleSides(num_sides)
{
    if (num_sides <= 2)
    {
        throw std::runtime_error("Polygon must have at least 3 sides");
    }

    m_vertices[0] = 0;
    m_vertices[1] = 0;
    m_vertices[2] = 0;

    float angle = (2.f * 3.141592f) / num_sides;
    for(int i = 0; i < num_sides + 1; ++i)
    {
        m_vertices[(i+1) * 3] = cos(angle * i) / 2.f;
        m_vertices[(i+1) * 3 + 1] = sin(angle * i) / 2.f;
        m_vertices[(i+1) * 3 + 2] = 0;
    }
}

//-------------------------------------------------------------------------------------------------

void Polygon::render() const
{
    glDrawArrays(GL_TRIANGLE_FAN, 0, std::min(m_numSides, m_numVisibleSides)+2);
}

//-------------------------------------------------------------------------------------------------

void Polygon::on_bind(Lua& lua, Lua::Object& object)
{
    Shape::on_bind(lua, object);
    object.set("set_num_visible_sides", [&](int num) {set_num_visible_sides(num);});
}

//-------------------------------------------------------------------------------------------------

void Polygon::info()
{
    Log::title("Object", "Polygon");
    Log::info(description());
    Log::info("");
    Log::info("Functions:");
    Log::func("set_num_visible_sides", {"N"}, "Draws only a portion of the polygon, up to N sides");
    Log::info("Is a Shape Object:");
    Shape::info();
}

//-------------------------------------------------------------------------------------------------
}//pi_kit
