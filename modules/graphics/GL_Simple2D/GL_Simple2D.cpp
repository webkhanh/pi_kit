#include "GL_Simple2D.h"
#include "Shader.h"
#include "Box.h"
#include "Polygon.h"
#include "Star.h"
#include "Line.h"

#include <bcm_host.h>

namespace pi_kit {

static bool s_instantiated = false;

//-------------------------------------------------------------------------------------------------

GL_Simple2D::GL_Simple2D()
{
    if (s_instantiated)
    {
        throw std::runtime_error("Only 1 GL-based module possible");
    }
    s_instantiated = true;

    bcm_host_init();
    i2 screen_size = get_screen_size();

    uint32_t width = static_cast<uint32_t>(screen_size.width);
    uint32_t height = static_cast<uint32_t>(screen_size.height);

    int32_t success = 0;
    EGLBoolean result;
    EGLint num_config;

    static EGL_DISPMANX_WINDOW_T native_window;

    DISPMANX_ELEMENT_HANDLE_T dispman_element;
    DISPMANX_DISPLAY_HANDLE_T dispman_display;
    DISPMANX_UPDATE_HANDLE_T dispman_update;
    VC_RECT_T dst_rect;
    VC_RECT_T src_rect;

    static const EGLint attribute_list[] =
    {
        EGL_RED_SIZE, 8,
        EGL_GREEN_SIZE, 8,
        EGL_BLUE_SIZE, 8,
        EGL_ALPHA_SIZE, 8,
        EGL_DEPTH_SIZE, 16,   // You need this line for depth buffering to work
        EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
        EGL_NONE
    };

    static const EGLint context_attributes[] =
    {
        EGL_CONTEXT_CLIENT_VERSION, 2,
        EGL_NONE
    };
    EGLConfig config;

    m_display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    if (glGetError() != 0)
    {
        throw std::runtime_error("Cannot get EGL display");
    }

    result = eglInitialize(m_display, NULL, NULL);
    if (glGetError() != 0)
    {
        throw std::runtime_error("Cannot initialize EGL");
    }
    

    // get an appropriate EGL frame buffer configuration
    result = eglChooseConfig(m_display, attribute_list, &config, 1, &num_config);
    if (glGetError() != 0)
    {
        throw std::runtime_error("Cannot choose EGL config");
    }

    // get an appropriate EGL frame buffer configuration
    result = eglBindAPI(EGL_OPENGL_ES_API);
    if (glGetError() != 0)
    {
        throw std::runtime_error("Cannot bind EGL API");
    }

    // create an EGL rendering context
    m_context = eglCreateContext(m_display, config, EGL_NO_CONTEXT, context_attributes);
    if (glGetError() != 0)
    {
        throw std::runtime_error("Cannot create EGL context");
    }

    dst_rect.x = 0;
    dst_rect.y = 0;
    dst_rect.width = width;
    dst_rect.height = height;

    src_rect.x = 0;
    src_rect.y = 0;
    src_rect.width = width << 16;
    src_rect.height = height << 16;

    dispman_display = vc_dispmanx_display_open( 0 /* LCD */);
    dispman_update = vc_dispmanx_update_start( 0 );

    dispman_element = vc_dispmanx_element_add ( dispman_update, dispman_display,
        0/*layer*/, &dst_rect, 0/*src*/,
        &src_rect, DISPMANX_PROTECTION_NONE, 0 /*alpha*/, 0/*clamp*/, (DISPMANX_TRANSFORM_T)0/*transform*/);

    native_window.element = dispman_element;
    native_window.width = width;
    native_window.height = height;
    vc_dispmanx_update_submit_sync(dispman_update);

    m_surface = eglCreateWindowSurface( m_display, config, &native_window, NULL );
    if (glGetError() != 0)
    {
        throw std::runtime_error("Cannot create window surface");
    }

    // connect the context to the surface
    result = eglMakeCurrent(m_display, m_surface, m_surface, m_context);
    if (glGetError() != 0)
    {
        throw std::runtime_error("Cannot set EGL context");
    }

    // Set background color and clear buffers
    glClearColor(0.f, 0.f, 0.f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT);

    glViewport(0, 0, width, height);

    m_shader.reset(new Shader());
}

//-------------------------------------------------------------------------------------------------

GL_Simple2D::~GL_Simple2D()
{
    eglMakeCurrent(m_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
    eglDestroySurface(m_display, m_surface);
    eglDestroyContext(m_display, m_context);

    eglTerminate(m_display);

    bcm_host_deinit();

    s_instantiated = false;
}

//-------------------------------------------------------------------------------------------------

void GL_Simple2D::on_error()
{
    reset(); //clears surface
}

//-------------------------------------------------------------------------------------------------

void GL_Simple2D::reset()
{
    m_shapes.clear();
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT);
    eglSwapBuffers(m_display, m_surface);
}

//-------------------------------------------------------------------------------------------------

void GL_Simple2D::on_frame() 
{
    if (m_shapes.empty())
    {
        return;
    }

    glClearColor(0.f, 0.f, 0.f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT);
    for(auto shape: m_shapes)
    {
        if (shape->is_visible())
        {        
            shape->setup_shaders(m_ratio, m_shader->id());
            shape->render();
        }
    }
    eglSwapBuffers(m_display, m_surface);
}

//-------------------------------------------------------------------------------------------------

i2 GL_Simple2D::get_screen_size()
{
    uint32_t width;
    uint32_t height;

    // create an EGL window surface
    int32_t success = graphics_get_display_size(0 /* LCD */, &width, &height);
    assert( success >= 0 );
    return i2(static_cast<int>(width), static_cast<int>(height));
}

//-------------------------------------------------------------------------------------------------

float GL_Simple2D::get_aspect_ratio()
{
    return m_ratio;
}

//-------------------------------------------------------------------------------------------------

std::shared_ptr<Box> GL_Simple2D::create_box(const float width, const float height)
{
    auto shape = std::make_shared<Box>();
    m_shapes.push_back(shape);
    shape->set_size(v2(width, height));
    return shape;
}

//-------------------------------------------------------------------------------------------------

std::shared_ptr<Polygon> GL_Simple2D::create_poly(const int num_sides)
{
    auto shape = std::make_shared<Polygon>(num_sides);
    m_shapes.push_back(shape);
    return shape;
}

//-------------------------------------------------------------------------------------------------

std::shared_ptr<Star> GL_Simple2D::create_star(const int num_points, const float concavity)
{
    auto shape = std::make_shared<Star>(num_points, concavity);
    m_shapes.push_back(shape);
    return shape;
}

//-------------------------------------------------------------------------------------------------

std::shared_ptr<Line> GL_Simple2D::create_line(const float x1, const float y1, const float x2, const float y2)
{
    auto shape = std::make_shared<Line>(v2(x1, y1), v2(x2, y2));
    m_shapes.push_back(shape);
    return shape;
}

//-------------------------------------------------------------------------------------------------

void GL_Simple2D::on_bind(Lua& lua, Lua::Object& object)
{
    Bindable::on_bind(lua, object);

    object.set("get_ratio", [&]() -> float {
        return get_aspect_ratio();
    });
    object.set("set_ratio", [&](float ratio) {
        set_aspect_ratio(ratio);
    });
    object.set("create_box", [&](const float width, const float height)->Lua::Object
    {
        try
        {
            auto shape = create_box(width == 0 ? 1 : width, height == 0 ? 1 : height);
            return lua.create_and_bind_object(shape);
        }
        catch(std::exception& e) {lua.throw_error(e.what()); return Lua::Object();}
    });
    object.set("create_poly", [&](const int numSides)->Lua::Object
    {
        try
        {
            auto shape = create_poly(numSides == 0 ? 3 : numSides);
            return lua.create_and_bind_object(shape);
        }
        catch(std::exception& e) {lua.throw_error(e.what()); return Lua::Object();}
    });
    object.set("create_star", [&](const int numPoints, const float ratio)->Lua::Object
    {
        try
        {
            auto shape = create_star(numPoints == 0 ? 5 : numPoints, ratio == 0 ? 0.382 : ratio);
            return lua.create_and_bind_object(shape);
        }
        catch(std::exception& e) {lua.throw_error(e.what()); return Lua::Object();}
    });
    object.set("create_line", [&](float x1, float y1, float x2, float y2)->Lua::Object
    {
        try
        {
            if (x1 == 0 && y1 == 0 && x2 == 0 && y2 == 0)
            {
                x1 = -0.5;
                x2 = 0.5;
            }
            auto shape = create_line(x1, y1, x2, y2);
            return lua.create_and_bind_object(shape);
        }
        catch(std::exception& e) {lua.throw_error(e.what()); return Lua::Object();}
    });
}

//-------------------------------------------------------------------------------------------------

void GL_Simple2D::info()
{
    Log::title("Module", "GL_Simple2D");
    Log::info(description());
    Log::info("Provides a 2D orthogonal plane onto which geometric shapes can be created and manipulated");
    Log::info("");
    Log::info("Defaults:");
    Log::info("Screen aspect ratio", "16/9 = 1.777...");
    Log::info("Origin", "Screen center (x = [-ratio .. +ratio], y = [-1 .. +1])");
    Log::info("Frame rate", "60 FPS");
    Log::info("");
    Log::info("Functions:");
    Log::func("get_ratio", {}, "Returns screen width/height aspect ratio");
    Log::func("set_ratio", {"ratio"}, "Sets screen width/height aspect ratio");
    Log::func("create_box", {"width (default: 1)", "height (default: 1)"}, "Creates and returns a rectangle shape");
    Log::func("create_poly", {"num. sides (default: 3)"}, "Creates and returns a regular polygon shape");
    Log::func("create_star", {"num. points (default: 5)", "\'concavity\' (default: 0.382)"}, "Creates and returns a regular star polygon shape");
    Log::func("create_line", {"x1 (default: -0.5)", "y1 (default: 0)", "x2 (default: 0.5)", "y2 (default: 0)"}, "Creates and returns a line shape");
    Log::info("Run .info() on shape objects for more info");

}

//-------------------------------------------------------------------------------------------------
}//pi_kit
