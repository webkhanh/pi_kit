#pragma once

#include "GL_Simple2D.h"

#include <apps/LuaApp/Bindable.h>

namespace pi_kit{

//-------------------------------------------------------------------------------------------------

class Shape: public Bindable
{

public:

    static const char* description()
    {
        return "2D geometrical shape to be used in the GL_Simple2D module";
    }

    Shape();

    v2 get_pos() const {return m_position;}

    void set_visible(bool visible) {m_visible = visible;}
    bool is_visible() const {return m_visible;}
    void set_pos(v2 position) {m_position = position;}
    void set_color(Color color) {m_color = color;}
    void set_scale(float scale) {m_scale.x = scale; m_scale.y = scale;}
    void set_scale(v2 scale) {m_scale = scale;}
    void set_rotation(float rotation){m_rotation = rotation;}
    void set_size(v2 size) {m_size = size;}

    v2 get_size() const {return m_size;}

    void setup_shaders(float aspectRatio, int shadersID);

    virtual float* get_vertices() const = 0;
    virtual void render() const = 0;

    void on_bind(Lua& lua, Lua::Object& value) override;
    void info() override;

private:

    bool m_visible = true;
    v2 m_position;
    v2 m_size;
    v2 m_scale;
    float m_rotation;
    Color m_color;

    float m_time = -1;

};

//-------------------------------------------------------------------------------------------------
}//pi_kit
