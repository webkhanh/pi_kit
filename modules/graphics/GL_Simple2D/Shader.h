#pragma once

#include "GL_Simple2D.h"

namespace pi_kit{

//-------------------------------------------------------------------------------------------------

class Shader
{

public:

	Shader();
    ~Shader();

    int id() const {return m_id;}

private:

	int attach_shader(const int program_id, const int type, const char* source);
	void detach_and_delete_shader(int program_id, int id);

    int m_id = 0;
    int m_vertex_id = 0;
    int m_fragment_id = 0;

};

//-------------------------------------------------------------------------------------------------
}//pi_kit
