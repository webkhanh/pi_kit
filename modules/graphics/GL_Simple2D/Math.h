#pragma once

#include <math.h>

namespace pi_kit{

//-------------------------------------------------------------------------------------------------

struct i2
{
    i2():x(0),y(0){}
    i2(int x, int y):x(x),y(y){}

    union
    {
        struct
        {
            int x;
            int y;
        };
        struct
        {
            int width;
            int height;
        };
    };

    inline bool operator == (const i2& i) const
    {
        return x == i.x && y == i.y;
    }

    inline i2 operator + (const i2& i) const
    {
        return i2(x+i.x, y+i.y);
    }

    inline i2 operator - (const i2& i) const
    {
        return i2(x-i.x, y-i.y);
    }

    inline bool operator != (const i2& i) const
    {
        return i.x != x || i.y !=y;
    }

    inline i2 operator * (const int k)
    {
        return i2(x * k, y * k);
    }

    inline i2 operator / (const int k)
    {
        return i2(x / k, y / k);
    }
};

//-------------------------------------------------------------------------------------------------

struct v2
{
    v2(){ x=0;y=0; }
    v2( i2 i ){ x=(float)i.x;y=(float)i.y; }

    union
    {
        struct
        {
            float x;
            float y;
        };
        struct
        {
            float w;
            float h;
        };
    };

    v2 ( float nx, float ny ):x(nx), y(ny){}

    inline static float dot( const v2& a, const v2& b )
    {
        return a.x * b.x + a.y * b.y;
    }

    inline void set_length( const float& length )
    {
        float currentLength = sqrt(length_squared());

        if ( currentLength > 0 )
        {
            *this *= length / currentLength;
        }
    }

    inline float length_squared() const
    {
        return dot( *this, *this );
    }

    inline v2 operator + ( const v2& v ) const
    {
        return v2( x + v.x, y + v.y );
    }

    inline v2 operator - ( const v2& v ) const
    {
        return v2( x - v.x, y - v.y );
    }

    inline bool operator == ( const v2& v ) const
    {
        return x == v.x && y == v.y;
    }

    inline bool operator != ( const v2& v ) const
    {
        return x != v.x || y != v.y;
    }

    inline v2& operator += ( const v2& v )
    {
        x += v.x;
        y += v.y;
        return *this;
    }

    inline v2& operator -= ( const v2& v )
    {
        x -= v.x;
        y -= v.y;
        return *this;
    }

    inline v2& operator *= ( const float k )
    {
        x *= k;
        y *= k;
        return *this;
    }

    inline v2& operator /= ( const float k )
    {
        x /= k;
        y /= k;
        return *this;
    }

    inline v2 operator * ( const float k )
    {
        return v2( x * k, y * k );
    }

    inline v2 operator / ( const float k )
    {
        return v2( x / k, y / k );
    }
};

//-------------------------------------------------------------------------------------------------

struct v3
{
    v3(){ x=0;y=0;z=0; }

    union
    {
        struct
        {
            float x;
            float y;
            float z;
        };
        float m[3];
    };

    v3 (float nx, float ny, float nz) : x(nx), y(ny), z(nz) {}

    inline static float dot( const v3& a, const v3& b )
    {
        return a.x * b.x + a.y * b.y + a.z * b.z;
    }

    inline static v3 cross_product( const v3& a, const v3& b )
    {
        return v3(
            a.y * b.z - a.z * b.y,
            a.z * b.x - a.x * b.z,
            a.x * b.y - a.y * b.x
        );
    }

    inline void set_length( const float& length )
    {
        float currentLength = sqrt(length_squared());

        if ( currentLength > 0 )
        {
            *this *= length / currentLength;
        }
    }
        
    inline float length_squared() const
    {
        return dot( *this, *this );
    }
    
    // operators
    inline v3 operator + ( const v3& _fx32 ) const
    {
        return v3( x + _fx32.x, y + _fx32.y, z + _fx32.z );
    }

    inline v3 operator - ( const v3& _fx32 ) const
    {
        return v3( x - _fx32.x, y - _fx32.y, z - _fx32.z );
    }

    inline v3 operator - (void) const
    {
        return v3( -x, -y, -z );
    }

    inline v3 operator * ( const float k ) const
    {
        return v3( x * k, y * k, z * k );
    }

    inline v3 operator * ( const v3& k ) const
    {
        return v3( x * k.x, y * k.y, z * k.z );
    }

    inline v3 operator / ( const float k ) const
    {
        return v3( x / k, y / k, z / k );
    }

    inline v3& operator += ( const v3& _fx32 )
    {
        x += _fx32.x;
        y += _fx32.y;
        z += _fx32.z;
        return *this;
    }

    inline v3& operator -= ( const v3& _fx32 )
    {
        x -= _fx32.x;
        y -= _fx32.y;
        z -= _fx32.z;
        return *this;
    }

    inline v3& operator *= ( const float k )
    {
        x *= k;
        y *= k;
        z *= k;
        return *this;
    }

    inline v3& operator /= ( const float k )
    {
        x /= k;
        y /= k;
        z /= k;
        return *this;
    }

    inline float operator [] (int idx) {return m[idx];} //!!!!
};

//-------------------------------------------------------------------------------------------------

struct m4
{
    m4()
    {
        m[0] = 1;
        m[1] = 0;
        m[2] = 0;
        m[3] = 0;
        m[4] = 0;
        m[5] = 1;
        m[6] = 0;
        m[7] = 0;
        m[8] = 0;
        m[9] = 0;
        m[10] = 1;
        m[11] = 0;
        m[12] = 0;
        m[13] = 0;
        m[14] = 0;
        m[15] = 1;
    }

    inline float operator [] (int idx) {return m[idx];} //!!!!

    void rotate_z(float angle)
    {
        float s = sin(angle),
            c = cos(angle),
            a00 = m[0],
            a01 = m[1],
            a02 = m[2],
            a03 = m[3],
            a10 = m[4],
            a11 = m[5],
            a12 = m[6],
            a13 = m[7];

        // Perform axis-specific matrix multiplication
        m[0] = a00 * c + a10 * s;
        m[1] = a01 * c + a11 * s;
        m[2] = a02 * c + a12 * s;
        m[3] = a03 * c + a13 * s;

        m[4] = a00 * -s + a10 * c;
        m[5] = a01 * -s + a11 * c;
        m[6] = a02 * -s + a12 * c;
        m[7] = a03 * -s + a13 * c;
    }

    void scale(v3 vec)
    {
        float x = vec[0], y = vec[1], z = vec[2];

        m[0] *= x;
        m[1] *= x;
        m[2] *= x;
        m[3] *= x;
        m[4] *= y;
        m[5] *= y;
        m[6] *= y;
        m[7] *= y;
        m[8] *= z;
        m[9] *= z;
        m[10] *= z;
        m[11] *= z;
    }

    void translate(v3 vec)
    {
        float x = vec[0], y = vec[1], z = vec[2];

        m[12] = m[0] * x + m[4] * y + m[8] * z + m[12];
        m[13] = m[1] * x + m[5] * y + m[9] * z + m[13];
        m[14] = m[2] * x + m[6] * y + m[10] * z + m[14];
        m[15] = m[3] * x + m[7] * y + m[11] * z + m[15];
    }

    float m[16];
};

//-------------------------------------------------------------------------------------------------

struct Color
{
    Color():r(0),g(0),b(0),a(0){}
    Color( float _r, float _g, float _b, float _a ):r(_r),g(_g),b(_b),a(_a){}

    union
    {
        struct
        {
            float r;
            float g;
            float b;
            float a;
        };
        float rgba[4];
    };
};

//-------------------------------------------------------------------------------------------------
}//pi_kit