#pragma once

#include "Shape.h"
#include "GL_Simple2D.h"

namespace pi_kit{

//-------------------------------------------------------------------------------------------------

class Star: public Shape
{
    static const char* description()
    {
        return "Regular Star-like Shape";
    }

public:

    Star(int numPoints, float ratio);

    float* get_vertices() const override {return m_vertices.get();}
    void render() const override;
    void info() override;
    
private:

    int m_num_points;
    std::unique_ptr<float[]> m_vertices;

};

//-------------------------------------------------------------------------------------------------
}//pi_kit
