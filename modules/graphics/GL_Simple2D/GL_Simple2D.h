#pragma once

#include "Math.h"

#include <common/Module.h>
#include <apps/LuaApp/Bindable.h>

#include <GLES2/gl2.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>

#include <string>
#include <memory>
#include <vector>

namespace pi_kit {

class Shape;
class Box;
class Polygon;
class Star;
class Line;
class Shader;

//-------------------------------------------------------------------------------------------------

class GL_Simple2D : public Module, public Bindable
{
public:

    static const char* description()
    {
        return "Minimalist 2D renderer via HDMI (GL_Simple2D - OpenGL ES 2.0 / vc4 / Legacy driver)";
    }

	GL_Simple2D();
	~GL_Simple2D();

	void on_frame() override;
	void reset() override;
	void on_bind(Lua& lua, Lua::Object&) override;
	void on_error() override;
	void info() override;

	i2 get_screen_size();
	float get_aspect_ratio();
	void set_aspect_ratio(float ratio)
	{
		m_ratio = ratio;
	}

	std::shared_ptr<Box> create_box(float width = 1.f, float height = 1.f);
	std::shared_ptr<Polygon> create_poly(int num_sides = 3);
	std::shared_ptr<Star> create_star(int num_points = 5, float concavity = 0.382f);
	std::shared_ptr<Line> create_line(float x1 = -0.5f, float y1 = 0, float x2 = 0.5f, float y2 = 0);	

private:

	EGLDisplay m_display;
	EGLSurface m_surface;
	EGLContext m_context;

	std::unique_ptr<Shader> m_shader;

	float m_ratio = 16.f/9.f;
	std::vector<std::shared_ptr<Shape>> m_shapes;

};

//-------------------------------------------------------------------------------------------------
}//pi_kit
