#include "Shape.h"

namespace pi_kit{

//-------------------------------------------------------------------------------------------------

Shape::Shape()
: m_position()
, m_size(1, 1)
, m_scale(1, 1)
, m_rotation(0)
, m_color(1, 1, 1, 1)
{
}

//-------------------------------------------------------------------------------------------------

void Shape::setup_shaders(float aspect_ratio, int shaders_id)
{
    float* vertices = get_vertices();

    glUseProgram(shaders_id);

    // vertices
    int position_slot = glGetAttribLocation(shaders_id, "position");
    glEnableVertexAttribArray(position_slot);
    glVertexAttribPointer(
        position_slot,
        3,           
        GL_FLOAT,    
        GL_FALSE,    
        0,           
        vertices
    );

    // color
    int color_slot = glGetUniformLocation(shaders_id, "color");
    glEnableVertexAttribArray(color_slot);
    auto color = m_color;
    color.a = 1.f; //todo: enable alpha
    glUniform4fv(color_slot, 1, color.rgba);

    // TSR
    int projection_slot = glGetUniformLocation(shaders_id, "projection");
    m4 projection;

    projection.scale(v3(1.f/aspect_ratio, 1, 1));

    projection.translate(v3(m_position.x, m_position.y, 0));
    projection.rotate_z(m_rotation);
    projection.scale(v3(m_scale.x, m_scale.y, 1));
    projection.scale(v3(m_size.x, m_size.y, 1));

    glUniformMatrix4fv(projection_slot, 1, 0, projection.m);
}

//-------------------------------------------------------------------------------------------------

void Shape::on_bind(Lua& lua, Lua::Object& object)
{
    Bindable::on_bind(lua, object);

    object.set("set_pos", [&](float x, float y) {return set_pos(v2(x, y)); });        
    object.set("set_RGB", [&](float r, float g, float b) {set_color(Color(r, g, b, 1));});
    object.set("set_scale", [&](float x, float y)
    {
        set_scale(v2(x, y == 0 ? x : y));
    });
    object.set("setAngle", [&](float r) {set_rotation(r);});
    object.set("set_size", [&](float x, float y) {set_size(v2(x, y));});
    object.set("set_visible", [&](bool set) {set_visible(set);});
}

//-------------------------------------------------------------------------------------------------

void Shape::info()
{
    Log::title("Object", "Shape");
    Log::info(description());
    Log::info("");
    Log::info("Defaults:");
    Log::info("Origin", "(0, 0) is at its visual center");
    Log::info("Color", "White (1, 1, 1)");
    Log::info("Position", "(0, 0)");
    Log::info("Scale", "(1, 1)");
    Log::info("Angle", "0 (radian)");
    Log::info("Size", "(1, 1)");
    Log::info("Visible", "true");
    Log::info("");
    Log::info("Functions:");
    Log::func("set_pos", {"x", "y"}, "Sets position of shape");
    Log::func("set_RGB", {"red (0..1)", "green (0..1)", "blue (0..1)"}, "Sets color components of the shape");
    Log::func("set_scale", {"x ratio", "y ratio (default: value of x)"}, "Sets scale of the shape. Use negative values to 'flip' shape");
    Log::func("setAngle", {"angle"}, "Sets shape angle of rotation (in radian). 2 x Pi = 360 degrees");
    Log::func("set_size", {"width", "height"}, "Sets size of shape");
    Log::func("set_visible", {"true / false"}, "Show / hide shape");
}


//-------------------------------------------------------------------------------------------------
}//pi_kit
