#pragma once

#include "Shape.h"
#include "GL_Simple2D.h"

namespace pi_kit{

//-------------------------------------------------------------------------------------------------

class Line: public Shape
{
    static const char* description()
    {
        return "1 pixel-thick 2D line Shape";
    }

public:

    Line(v2 start, v2 end);

    void set_points(v2 start, v2 end);

    float* get_vertices() const override {return m_vertices.get();}
    void render() const override;

    void on_bind(Lua& lua, Lua::Object& value) override;
    void info() override;

private:

    std::unique_ptr<float[]> m_vertices;

};

//-------------------------------------------------------------------------------------------------
}//pi_kit
