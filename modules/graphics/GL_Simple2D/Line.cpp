#include "Line.h"

namespace pi_kit{

//-------------------------------------------------------------------------------------------------

Line::Line(v2 start, v2 end)
: Shape()
{
	m_vertices.reset(new float[2 * 3]);

	m_vertices[0] = start.x;
	m_vertices[1] = start.y;
	m_vertices[2] = 0;
	m_vertices[3] = end.x;
	m_vertices[4] = end.y;
	m_vertices[5] = 0;
}

//-------------------------------------------------------------------------------------------------

void Line::set_points(v2 start, v2 end)
{
    m_vertices[0] = start.x;
    m_vertices[1] = start.y;
    m_vertices[3] = end.x;
    m_vertices[4] = end.y;
}

//-------------------------------------------------------------------------------------------------

void Line::render() const
{
    glDrawArrays(GL_LINES, 0, 2);
}

//-------------------------------------------------------------------------------------------------

void Line::on_bind(Lua& lua, Lua::Object& value)
{
    Shape::on_bind(lua, value);
    value.set("set_points", [&](float x1, float y1, float x2, float y2){ set_points(v2(x1, y1), v2(x2, y2)); });
}

//-------------------------------------------------------------------------------------------------

void Line::info()
{
    Log::title("Object", "Line");
    Log::info(description());
    Log::info("");
    Log::info("Functions:");
    Log::func("set_points", {"x1", "y1", "x2", "y2"}, "Sets positions (x1, y1) and (x2, y2) of end points of line.");
    Log::info("Is a Shape Object:");
    Shape::info();
}

//-------------------------------------------------------------------------------------------------
}//pi_kit
