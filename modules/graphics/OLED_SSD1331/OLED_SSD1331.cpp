#include "OLED_SSD1331.h"

#include <common/pins.h>

#include <wiringPi.h>
#include <wiringPiSPI.h>
#include <stdio.h>
#include <time.h>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

static const int WIDTH = 96;
static const int HEIGHT = 64;

#define DRAW_LINE                       0x21
#define DRAW_RECTANGLE                  0x22
#define COPY_WINDOW                     0x23
#define DIM_WINDOW                      0x24
#define CLEAR_WINDOW                    0x25
#define FILL_WINDOW                     0x26
#define DISABLE_FILL                    0x00
#define ENABLE_FILL                     0x01
#define CONTINUOUS_SCROLLING_SETUP      0x27
#define DEACTIVE_SCROLLING              0x2E
#define ACTIVE_SCROLLING                0x2F

#define SET_COLUMN_ADDRESS              0x15
#define SET_ROW_ADDRESS                 0x75
#define SET_CONTRAST_A                  0x81
#define SET_CONTRAST_B                  0x82
#define SET_CONTRAST_C                  0x83
#define MASTER_CURRENT_CONTROL          0x87
#define SET_PRECHARGE_SPEED_A           0x8A
#define SET_PRECHARGE_SPEED_B           0x8B
#define SET_PRECHARGE_SPEED_C           0x8C
#define SET_REMAP                       0xA0
#define SET_DISPLAY_START_LINE          0xA1
#define SET_DISPLAY_OFFSET              0xA2
#define NORMAL_DISPLAY                  0xA4
#define ENTIRE_DISPLAY_ON               0xA5
#define ENTIRE_DISPLAY_OFF              0xA6
#define INVERSE_DISPLAY                 0xA7
#define SET_MULTIPLEX_RATIO             0xA8
#define DIM_MODE_SETTING                0xAB
#define SET_MASTER_CONFIGURE            0xAD
#define DIM_MODE_DISPLAY_ON             0xAC
#define DISPLAY_OFF                     0xAE
#define NORMAL_BRIGHTNESS_DISPLAY_ON    0xAF
#define POWER_SAVE_MODE                 0xB0
#define PHASE_PERIOD_ADJUSTMENT         0xB1
#define DISPLAY_CLOCK_DIV               0xB3
#define SET_GRAy_SCALE_TABLE            0xB8
#define ENABLE_LINEAR_GRAY_SCALE_TABLE  0xB9
#define SET_PRECHARGE_VOLTAGE           0xBB

#define SET_V_VOLTAGE                   0xBE

namespace{

    void setupScreen(int pin_reset, int pin_dc, int channel)
    {
        auto command = [&](unsigned char cmd){
            digitalWrite(pin_dc, LOW);
            wiringPiSPIDataRW(channel, &cmd, 1);
        };

        pinMode(pin_reset, OUTPUT);
        pinMode(pin_dc, OUTPUT);
        wiringPiSPISetup(channel, 2000000);    //2M

        digitalWrite(pin_reset, HIGH);
        delay(10);
        digitalWrite(pin_reset, LOW);
        delay(10);
        digitalWrite(pin_reset, HIGH);

        command(DISPLAY_OFF);          //Display Off
        command(SET_CONTRAST_A);       //Set contrast for color A
        command(0xFF);                     //145 0x91
        command(SET_CONTRAST_B);       //Set contrast for color B
        command(0xFF);                     //80 0x50
        command(SET_CONTRAST_C);       //Set contrast for color C
        command(0xFF);                     //125 0x7D
        command(MASTER_CURRENT_CONTROL);//master current control
        command(0x06);                     //6
        command(SET_PRECHARGE_SPEED_A);//Set Second Pre-change Speed For ColorA
        command(0x64);                     //100
        command(SET_PRECHARGE_SPEED_B);//Set Second Pre-change Speed For ColorB
        command(0x78);                     //120
        command(SET_PRECHARGE_SPEED_C);//Set Second Pre-change Speed For ColorC
        command(0x64);                     //100
        command(SET_REMAP);            //set remap & data format
        command(0x72);                     //0x72              
        command(SET_DISPLAY_START_LINE);//Set display Start Line
        command(0x0);
        command(SET_DISPLAY_OFFSET);   //Set display offset
        command(0x0);
        command(NORMAL_DISPLAY);       //Set display mode
        command(SET_MULTIPLEX_RATIO);  //Set multiplex ratio
        command(0x3F);
        command(SET_MASTER_CONFIGURE); //Set master configuration
        command(0x8E);
        command(POWER_SAVE_MODE);      //Set Power Save Mode
        command(0x00);                     //0x00
        command(PHASE_PERIOD_ADJUSTMENT);//phase 1 and 2 period adjustment
        command(0x31);                     //0x31
        command(DISPLAY_CLOCK_DIV);    //display clock divider/oscillator frequency
        command(0xF0);
        command(SET_PRECHARGE_VOLTAGE);//Set Pre-Change Level
        command(0x3A);
        command(SET_V_VOLTAGE);        //Set vcomH
        command(0x3E);
        command(DEACTIVE_SCROLLING);   //disable scrolling
        command(NORMAL_BRIGHTNESS_DISPLAY_ON);//set display on        
    }

    void transferScreen(unsigned char *pBuffer, int size, int pin_dc, int channel)
    {
        auto command = [&](unsigned char cmd){
            digitalWrite(pin_dc, LOW);
            wiringPiSPIDataRW(channel, &cmd, 1);
        };
         
        int txLen = 512;
        int remain = size;
        command(SET_COLUMN_ADDRESS);
        command(0);         //cloumn start address
        command(WIDTH - 1); //cloumn end address
        command(SET_ROW_ADDRESS);
        command(0);         //page atart address
        command(HEIGHT - 1); //page end address
        digitalWrite(pin_dc, HIGH);
        while (remain > txLen)
        {
            wiringPiSPIDataRW(channel, pBuffer, txLen);
            remain -= txLen;
            pBuffer += txLen;
        }
        wiringPiSPIDataRW(channel, pBuffer, remain);
    }
}

//-------------------------------------------------------------------------------------------------

OLED_SSD1331::OLED_SSD1331(int pin_reset, int pin_dc, int channel)
: m_channel(channel)
{
    m_canvas.reset(new PixelCanvas16(WIDTH, HEIGHT));
    wiringPiSetup();
    setPins(pin_reset, pin_dc);
    set_spi(channel);
}

//-------------------------------------------------------------------------------------------------

OLED_SSD1331::~OLED_SSD1331()
{
    m_canvas->clear();
    refresh();
}

//-------------------------------------------------------------------------------------------------

void OLED_SSD1331::on_frame()
{
    if (m_canvas->modified())
    {
        refresh();
        m_canvas->reset_modified();
    }
}

//-------------------------------------------------------------------------------------------------

void OLED_SSD1331::refresh()
{
    transferScreen(m_canvas->get_buffer(), m_canvas->get_size(), pins::physical_to_WiringPi(m_pin_dc), m_channel);
}

//-------------------------------------------------------------------------------------------------

void OLED_SSD1331::setPins(int pin_reset, int pin_dc)
{
    m_pin_reset = pin_reset;
    m_pin_dc = pin_dc;

    if (!pins::is_output_capable(m_pin_reset))
    {
        throw std::runtime_error("Invalid RESET pin");
    }
    if (!pins::is_output_capable(m_pin_dc))
    {
        throw std::runtime_error("Invalid DC pin");
    }
}

//-------------------------------------------------------------------------------------------------

void OLED_SSD1331::set_spi(int channel)
{
    if (channel != 0 && channel != 1)
    {
        throw std::runtime_error("Invalid SPI channel (0 or 1 -> CE0 or CE1)");
    }

    m_channel = channel;
}

//-------------------------------------------------------------------------------------------------

void OLED_SSD1331::reset()
{
    setupScreen(pins::physical_to_WiringPi(m_pin_reset), pins::physical_to_WiringPi(m_pin_dc), m_channel);
    m_canvas->reset();
}

//-------------------------------------------------------------------------------------------------

void OLED_SSD1331::on_bind(Lua& lua, Lua::Object& object)
{
    Bindable::on_bind(lua, object);

    auto oled = lua.create_global("OLED");

    oled.set("BIG", (int)PixelCanvas16::BIG);
    oled.set("SMALL", (int)PixelCanvas16::SMALL);

    object.set("setPins", [&](int reset, int dc) {
        try
        {
            if (reset != m_pin_reset || dc != m_pin_dc)
            {
                setPins(reset, dc);
                setupScreen(pins::physical_to_WiringPi(m_pin_reset), pins::physical_to_WiringPi(m_pin_dc), m_channel);
            }
        }
        catch(std::exception &e) {lua.throw_error(e.what());}
    });
    object.set("set_spi", [&](int channel) {
        try
        {
            if (channel != m_channel)
            {
                set_spi(channel);
                setupScreen(pins::physical_to_WiringPi(m_pin_reset), pins::physical_to_WiringPi(m_pin_dc), m_channel);
            }
        }
        catch(std::exception &e) {lua.throw_error(e.what());}
    });

    object.set("set_text_size", [&](int size) {m_canvas->set_text_size(size);} );
    object.set("set_text_RGB", [&](float r, float g, float b) {m_canvas->set_text_RGB(r, g, b);} );
    object.set("set_text_pos", [&](int x, int y) {m_canvas->set_text_pos(x, y);} );
    object.set("set_text_invert", [&](bool invert) {m_canvas->set_text_invert(invert);} );
    object.set("set_text_wrap", [&](bool wrap) {m_canvas->set_text_wrap(wrap);} );
    object.set("print", [&](const char * str) {m_canvas->print(str ? str : "");});

    object.set("pixel", [&](int x, int y, float r, float g, float b) {m_canvas->pixel(x, y, r, g, b);} );
    object.set("clear", [&]() {m_canvas->clear();} );
}

//-------------------------------------------------------------------------------------------------

void OLED_SSD1331::info()
{
    Log::title("Module", "OLED_SSD1331");
    Log::info(description());
    Log::GPIO();
    std::string connections = "VCC: +3V3, SCL->SCLK: 23, SDA->MOSI: 19, RES: " + std::to_string(m_pin_reset) + " (configurable), DC: " + std::to_string(m_pin_dc) + " (configurable), CS: 24(CE0) or 26(CE1)";
    Log::info("SPI0/CE0 connections", connections.c_str());
    Log::info("SPI Channel", m_channel == 0 ? "CE0" : "CE1");
    Log::info("");
    Log::info("Values:");
    Log::info("Text sizes", "OLED.SMALL, OLED.BIG");
    Log::info("");
    Log::info("Functions:");
    Log::func("setPins", {"reset pin", "dc pin"}, "Sets screen to use those reset & dc pins");
    Log::func("set_spi", {"channel (0 or 1)"}, "Sets SPI channel to use for screen (CE0/CE1)");
    Log::func("set_text_size", {"size"}, "Sets the current text size. Only sizes 12 (OLED.SMALL) and 16 (OLED.BIG) supported");
    Log::func("set_text_RGB", {"r", "g", "b"}, "Sets the current text red, green and blue values [0..1]");
    Log::func("set_text_pos", {"x", "y"}, "Sets the current 'cursor' position");
    Log::func("set_text_invert", {"true / false"}, "In 'inverted' mode, the color affects the text background, while the foreground color is black");
    Log::func("set_text_wrap", {"true / false"}, "In 'wrapped' mode, text is displayed on the next line in case of screen overflow");
    Log::func("print", {"text"}, "Prints text on screen according to current settings");
    Log::func("pixel", {"x", "y", "r", "g", "b"}, "Displays a pixel on the screen. r, g, b [0..1]");
    Log::func("clear", {}, "Clears the screen");
}

//-------------------------------------------------------------------------------------------------
}//pi_kit
