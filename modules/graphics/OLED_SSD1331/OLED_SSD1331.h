#pragma once

#include <common/Module.h>
#include <common/graphics/PixelCanvas16.h>

#include <apps/LuaApp/Bindable.h>

#include <memory>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

class OLED_SSD1331: public Module, public Bindable
{
public:

    static const char* description()
    {
        return "OLED screen module (OLED_SSD1331 - 96x64 RGB SSD1331 OLED)";
    }

	OLED_SSD1331(int pin_reset = 35, int pin_dc = 36, int channel = 0);
	~OLED_SSD1331();
	
	void on_bind(Lua& lua, Lua::Object& object) override;
	void info() override;
	void reset() override;
    void on_frame() override;

	void setPins(int pin_reset, int pin_dc);
	void set_spi(int channel);

	PixelCanvas16& getCanvas() const
	{
		return *m_canvas.get();
	}

	void refresh();

private:

	int m_pin_reset = 35;
	int m_pin_dc = 36;
	int m_channel = 0;

	std::unique_ptr<PixelCanvas16> m_canvas;
};

//-------------------------------------------------------------------------------------------------
}//pi_kit
