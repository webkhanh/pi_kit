#pragma once

#include <common/Module.h>
#include <apps/LuaApp/Bindable.h>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

class LEDStrip_WS281x: public Module, public Bindable
{
public:

    static const char* description()
    {
        return "LED strip module (LEDStrip_WS281x - 'Neopixel' LED strips such as WS2811, WS2812, WS2812B, WS2813, WS2815. Hardware PWM(0) driven). App must be run with sudo privileges";
    }

	LEDStrip_WS281x(int num_leds = 60);
	~LEDStrip_WS281x();
	
	void on_bind(Lua& lua, Lua::Object& object) override;
	void info() override;
	void reset() override;
    void on_frame() override;

    void set_RGB(int led, float r, float g, float b);
    void clear();
    void refresh();

private:

	bool m_modified = false;
    int m_num_leds = 0;
};

//-------------------------------------------------------------------------------------------------
}//pi_kit
