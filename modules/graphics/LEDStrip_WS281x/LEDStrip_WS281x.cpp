#include "LEDStrip_WS281x.h"
#include <rpi_ws281x/ws2811.h>

namespace pi_kit {

ws2811_t s_leds;
static bool s_instantiated = false;

//-------------------------------------------------------------------------------------------------

LEDStrip_WS281x::LEDStrip_WS281x(int num_leds)
: m_num_leds(num_leds)
{
    if (s_instantiated)
    {
        throw std::runtime_error("Only 1 LED module possible");
    }
    s_instantiated = true;

    s_leds.freq = WS2811_TARGET_FREQ;
    s_leds.dmanum = 10;
    
    s_leds.channel[0].gpionum = 18;
    s_leds.channel[0].count = m_num_leds;
    s_leds.channel[0].invert = 0;
    s_leds.channel[0].brightness = 255;
    s_leds.channel[0].strip_type = WS2811_STRIP_RGB;

    s_leds.channel[1].gpionum = 0;
    s_leds.channel[1].count = 0;
    s_leds.channel[1].invert = 0;
    s_leds.channel[1].brightness = 0;

    if (ws2811_init(&s_leds) != WS2811_SUCCESS)
    {
        throw std::runtime_error("Could not initialise LED Strip");
    }

    clear();
    refresh();
}

//-------------------------------------------------------------------------------------------------

LEDStrip_WS281x::~LEDStrip_WS281x()
{
    clear();
    refresh();
	ws2811_fini(&s_leds);
    s_instantiated = false;
}

//-------------------------------------------------------------------------------------------------

void LEDStrip_WS281x::reset()
{
    clear();
}

//-------------------------------------------------------------------------------------------------

void LEDStrip_WS281x::clear()
{
    for (int i = 0; i< m_num_leds; ++i)
    {
    	s_leds.channel[0].leds[i] = 0;
    }

    m_modified = true;
}

//-------------------------------------------------------------------------------------------------

void LEDStrip_WS281x::set_RGB(int led, float r, float g, float b)
{
    if (led < 0 || led >= m_num_leds)
    {
        return;
    }

    r = std::min(std::max(r, 0.f), 1.f);
    g = std::min(std::max(g, 0.f), 1.f);
    b = std::min(std::max(b, 0.f), 1.f);

    int red = int(r * 255) << 16;
	int green = int(g * 255) << 8;
	int blue = int(b * 255);
	s_leds.channel[0].leds[led] = red + green + blue;

    m_modified = true;
}

//-------------------------------------------------------------------------------------------------

void LEDStrip_WS281x::on_frame()
{
    if (m_modified)
    {
        refresh();
        m_modified = false;
    }
}

//-------------------------------------------------------------------------------------------------

void LEDStrip_WS281x::refresh()
{
    if (ws2811_render(&s_leds) != WS2811_SUCCESS)
    {
        throw std::runtime_error("Could not refresh LED Strip");
    }
}

//-------------------------------------------------------------------------------------------------

void LEDStrip_WS281x::on_bind(Lua& lua, Lua::Object& object)
{
    Bindable::on_bind(lua, object);

    object.set("set", [&](float led, float r, float g, float b) {
        try
        {
        	set_RGB(static_cast<int>(led), r, g, b);
        }
        catch(std::exception &e) {lua.throw_error(e.what());}
    });

    object.set("set_grb", [&](float led, float r, float g, float b) {
        try
        {
            set_RGB(static_cast<int>(led), g, r, b);
        }
        catch(std::exception &e) {lua.throw_error(e.what());}
    });

    object.set("clear", [&]() {
        try
        {
            clear();
        }
        catch(std::exception &e) {lua.throw_error(e.what());}
    });
}

//-------------------------------------------------------------------------------------------------

void LEDStrip_WS281x::info()
{
    Log::title("Module", "LEDStrip_WS281x");
    Log::info(description());
    Log::info("Connection via PWM0 only", "Pin #12");
    Log::info("Max. number of LEDs", std::to_string(m_num_leds).c_str());
    Log::info("Functions:");
    Log::func("set", {std::string("LED (0.." + std::to_string(m_num_leds-1) + ")").c_str(), "red (0..1)", "green (0..1)", "blue (0..1)"}, "Sets the RGB color components for the LED");
    Log::func("set_grb", {std::string("LED (0.." + std::to_string(m_num_leds-1) + ")").c_str(), "red (0..1)", "green (0..1)", "blue (0..1)"}, "Sets the RGB color components for the LED (GRB-type strips)");
    Log::func("clear", {}, "Turns off all LEDs");
}

//-------------------------------------------------------------------------------------------------
}//pi_kit
