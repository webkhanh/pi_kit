#pragma once

#include <common/Module.h>
#include <common/audio/Audio.h>
#include <apps/LuaApp/Bindable.h>


namespace pi_kit {

//-------------------------------------------------------------------------------------------------

class DSP_Sine: public Module, public Bindable
{
public:

    static const char* description()
    {
        return "16 channel Sine wave synth (DSP_Sine - Portaudio)";
    }

	using Callback = std::function<void(const void* in, void* out, int size)>;

    DSP_Sine();

    void reset() override;
    void on_bind(Lua& lua, Lua::Object& object) override;
    void info() override;

    void set_device(int id);

    void play_note(unsigned char note, unsigned char channel = 0);
    void play_tone(float freq, unsigned char channel = 0);

    void stop(unsigned char channel = 0);
    void set_frequency(float freq, unsigned char channel = 0)
    {
        if (channel > 15)
        {
            throw std::runtime_error("Invalid channel");
        }

    	m_sines[channel].m_freq = freq;
    }
    void set_gain(float gain, unsigned char channel = 0)
    {
        if (channel > 15)
        {
            throw std::runtime_error("Invalid channel");
        }
    	m_sines[channel].m_gain = gain;
    }

    void set_pan(float pan, unsigned char channel = 0)
    {
        if (channel > 15)
        {
            throw std::runtime_error("Invalid channel");
        }
        m_sines[channel].m_pan = pan;
    }

private:

    struct Sine
    {
        bool m_playing = false;
        float m_phase = 0;
        float m_gain = 0.5f;
        float m_pan = 0;
        float m_freq = 440;
    };

    std::unique_ptr<Audio> m_audio;
    Sine m_sines[16];
};

//-------------------------------------------------------------------------------------------------
}//pi_kit
