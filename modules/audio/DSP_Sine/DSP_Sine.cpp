#include "DSP_Sine.h"
#include "note_to_freq.h"

#include <iostream> //cout

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

DSP_Sine::DSP_Sine()
{
    m_audio.reset(new Audio());

    m_audio->set_events_callback([&](const void* in, void* out, int size){

        float val_left = 0;
        float val_right = 0;

        for (int i = 0; i < 16; ++i)
        {
            auto& sine = m_sines[i];
            if (sine.m_playing)
            {
                sine.m_phase += sine.m_freq * 2 * M_PI / m_audio->get_sample_rate();
                if (sine.m_phase > 2 * M_PI)
                {
                    sine.m_phase -= 2 * M_PI;
                }
                const float val = sin(sine.m_phase) * sine.m_gain;

                const float pan_left = std::min(std::max(-sine.m_pan / 2.f + 0.5f, 0.f), 1.f);
                const float pan_right = std::min(std::max(sine.m_pan / 2.f + 0.5f, 0.f), 1.f);
                val_left += val * pan_left;
                val_right += val * pan_right;
            }
        }

        float* fout = (float*)out;
        fout[0] = val_left; //left
        fout[1] = val_right; //right
    });

    m_audio->start(32, 44100);
}

//-------------------------------------------------------------------------------------------------

void DSP_Sine::reset()
{
    for (int i = 0; i < 16; ++i)
    {
        m_sines[i].m_playing = false;
    }
}

//-------------------------------------------------------------------------------------------------

void DSP_Sine::set_device(int id)
{
    m_audio->start(32, 44100, id);
}

//-------------------------------------------------------------------------------------------------

void DSP_Sine::play_note(unsigned char note, unsigned char channel)
{
    if (note > 127)
    {
        throw std::runtime_error("Invalid note");
    }

    play_tone(s_note_to_freq_440[note], channel);
}

//-------------------------------------------------------------------------------------------------

void DSP_Sine::play_tone(float freq, unsigned char channel)
{
    if (channel > 15)
    {
        throw std::runtime_error("Invalid channel");
    }
    m_sines[channel].m_freq = freq;
    m_sines[channel].m_playing = true;
}

//-------------------------------------------------------------------------------------------------

void DSP_Sine::stop(unsigned char channel)
{
    if (channel > 15)
    {
        throw std::runtime_error("Invalid channel");
    }
    m_sines[channel].m_playing = false;
}

//-------------------------------------------------------------------------------------------------

void DSP_Sine::on_bind(Lua& lua, Lua::Object& object)
{
    Bindable::on_bind(lua, object);

    object.set("set_device", [&](int device) {
        try
        {
            set_device(device);
        }
        catch(std::exception& e) {lua.throw_error(e.what());}        
    });    
    object.set("play_tone", [&](float freq, int channel) {
        try
        {
            play_tone(freq, channel ? channel - 1 : 1);
        }
        catch(std::exception& e) {lua.throw_error(e.what());}        
    });
    object.set("play_note", [&](int note, int channel) {
        try
        {
            play_note((unsigned char)note, channel ? channel - 1 : 1);
        }
        catch(std::exception& e) {lua.throw_error(e.what());}        
    });
    object.set("stop", [&](int channel) {
        try
        {
            stop(channel ? channel - 1 : 1);
        }
        catch(std::exception& e) {lua.throw_error(e.what());}        
    });
    object.set("set_frequency", [&](float f, int channel) {
        try
        {
            set_frequency(f, channel ? channel - 1 : 1);
        }
        catch(std::exception& e) {lua.throw_error(e.what());}        
    });
    object.set("set_gain", [&](float g, int channel) {
        try
        {
            set_gain(g, channel ? channel - 1 : 1);
        }
        catch(std::exception& e) {lua.throw_error(e.what());}        
    });
    object.set("set_pan", [&](float pan, int channel) {
        try
        {
            set_pan(pan, channel ? channel - 1 : 1);
        }
        catch(std::exception& e) {lua.throw_error(e.what());}        
    });
}

//-------------------------------------------------------------------------------------------------

void DSP_Sine::info()
{
    Log::title("Module", "DSP_Sine");
    Log::info(description());
    Log::info("");
    Log::info("Devices:");
    m_audio->list_devices();
    Log::info("");
    Log::info("Functions:");
    Log::func("set_device", {"id"}, "Changes the output device from default to 'id'. See above for a list of available devices");
    Log::func("play_tone", {"frequency in Hz", "channel (default: 1)"}, "Starts playing tone");
    Log::func("play_note", {"note (0..127)", "channel (default: 1)"}, "Starts playing note");
    Log::func("stop", {"channel (default: 1)"}, "Stops channel");
    Log::func("set_frequency", {"frequency in Hz", "channel (default: 1)"}, "Sets tone frequency");
    Log::func("set_pan", {"amount (-1..1)", "channel (default: 1)"}, "Sets tone panning. 0 = center");
    Log::func("set_gain", {"gain", "channel (default: 1)"}, "Sets tone gain (0..1)");
}

//-------------------------------------------------------------------------------------------------
}//pi_kit
