#include "WAV_Streamer.h"

namespace pi_kit {

struct WAV_fmt_descriptor
{
    //char fmt_header[4]; // Contains "fmt " (includes trailing space)
    int fmt_chunk_size; // Should be 16 for PCM
    short audio_format; // Should be 1 for PCM. 3 for IEEE Float
    short num_channels;
    int sample_rate;
    int byte_rate; // Number of bytes per second. sample_rate * num_channels * Bytes Per Sample
    short sample_alignment; // num_channels * Bytes Per Sample
    short bit_depth; // Number of bits per sample
    // Data
    char data_header[4]; // Contains "data"
    int data_bytes; // Number of bytes in data. Number of samples * num_channels * sample byte size
    // uint8_t bytes[]; // Remainder of wave file is bytes
};

//-------------------------------------------------------------------------------------------------

WAV_Streamer::WAV_Streamer()
{
    m_audio.reset(new Audio());

    m_audio->set_events_callback([&](const void* in, void* out, int frame_size){

        if (m_lock.load())
        {
            return;
        }

        m_lock.store(true);

        if (m_file.is_open())
        {
            if (m_file.read((char*)out, frame_size))
            {
                m_file_data_play_pos += frame_size;

                if (m_file_data_play_pos >= m_file_data_size)
                {
                    if (m_loop)
                    {
                        m_file_data_play_pos = 0;
                        m_file.seekg(m_file_data_start);
                    }
                    else
                    {
                        m_file.close();
                    }
                }
            }
            else // corrupt file
            {
                m_file.close();
            }
        }

        m_lock.store(false);
    });

    m_lock.store(false);

    m_audio->start(16, 44100, Audio::DEFAULT);
}

//-------------------------------------------------------------------------------------------------

void WAV_Streamer::stop()
{
    while(m_lock.load());

    m_lock.store(true);

    if (m_file.is_open())
    {
        m_file.close();
    }
    m_file_data_size = 0;
    m_file_data_start = 0;
    m_file_data_play_pos = 0;

    m_lock.store(false);
}

//-------------------------------------------------------------------------------------------------

void WAV_Streamer::play_file(std::string filename)
{
    while(m_lock.load());

    m_lock.store(true);

    if (m_file.is_open())
    {
        m_file.close();
    }

    m_file.open(filename);
    if (!m_file.is_open())
    {
        throw std::runtime_error("Cannot find file: " + filename);
    }

    // jump to the "fmt " descriptor
    char buff[4];
    do
    {
        if (!m_file.read(buff, 4))
        {
            throw std::runtime_error("Invalid WAV file: " + filename);
        }
    }
    while(strncmp(buff, "fmt ", 4) != 0);

    WAV_fmt_descriptor header;
    if (!m_file.read((char*)&header, sizeof(WAV_fmt_descriptor)))
    {
        throw std::runtime_error("Invalid WAV file: " + filename);
    }

    m_file_data_size = header.data_bytes;
    m_file_data_start = m_file.tellg();
    m_file_data_play_pos = 0;

    m_lock.store(false);
}

//-------------------------------------------------------------------------------------------------

void WAV_Streamer::reset()
{
    stop();
    m_loop = false;
}

//-------------------------------------------------------------------------------------------------

void WAV_Streamer::set_device(int id)
{
    m_audio->start(16, 44100, id);
}

//-------------------------------------------------------------------------------------------------

void WAV_Streamer::on_bind(Lua& lua, Lua::Object& object)
{
    Bindable::on_bind(lua, object);

    object.set("set_device", [&](int device) {
        try
        {
            set_device(device);
        }
        catch(std::exception& e) {lua.throw_error(e.what());}        
    }); 
    object.set("play_file", [&](const char * file) {
        try
        {
            play_file(file ? file : "");
        }
        catch(std::exception& e) {lua.throw_error(e.what());}
    });
    object.set("stop", [&]() {stop();});
    object.set("set_loop", [&](bool loop) {set_loop(loop);});
    object.set("get_file_size", [&]() -> size_t {return get_file_size();});
    object.set("get_file_pos", [&]() -> size_t {return get_file_pos();});
}

//-------------------------------------------------------------------------------------------------

void WAV_Streamer::info()
{
    Log::title("Module", "WAV_Streamer");
    Log::info(description());
    Log::info("");
    Log::info("Devices:");
    m_audio->list_devices();
    Log::info("");
    Log::info("Functions:");
    Log::func("set_device", {"id"}, "Changes the output device from default to 'id'. See above for a list of available devices");
    Log::func("play_file", {"file name"}, "Plays a PCM stereo 44.1kHz 16 bits LE .WAV file");
    Log::func("stop", {}, "Stops file playback");
    Log::func("set_loop", {"true / false"}, "Sets whether file should play in loop");
    Log::func("get_file_size", {""}, "Returns file size if currently playing. Returns 0 otherwise");
    Log::func("get_file_pos", {""}, "Returns current position in file if playing. Returns 0 otherwise");
}

//-------------------------------------------------------------------------------------------------
}//pi_kit
