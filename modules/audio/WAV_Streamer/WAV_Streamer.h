#pragma once

#include <apps/LuaApp/Bindable.h>
#include <common/Module.h>
#include <common/audio/Audio.h>

#include <fstream>      // std::ifstream
#include <atomic>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------


class WAV_Streamer: public Module, public Bindable
{
public:

    static const char* description()
    {
        return "PCM stereo .wav file streamer (WAV_Streamer)";
    }

    WAV_Streamer();

    void reset() override;
    void on_bind(Lua& lua, Lua::Object& object) override;
    void info() override;

    void set_device(int id);
    void play_file(std::string file);
    void stop();
    void set_loop(bool loop) {m_loop = loop;}

    size_t get_file_size() const {return m_file_data_size;}
    size_t get_file_pos() const {return m_file_data_play_pos;}

private:

    bool m_loop = false;
    std::atomic<bool> m_lock;

    std::ifstream m_file;
    size_t m_file_data_play_pos = 0;
    size_t m_file_data_start = 0;
    size_t m_file_data_size = 0;
    
    std::unique_ptr<Audio> m_audio;
};

//-------------------------------------------------------------------------------------------------
}//pi_kit
