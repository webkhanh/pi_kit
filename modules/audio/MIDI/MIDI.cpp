#include "MIDI.h"

#include "MIDIInputPort.h"

#include <iostream> //cout

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

namespace {

	void for_each_card(std::function<void(int)> func)
	{
	    int card_id = -1;
	    while(snd_card_next(&card_id) >= 0)
	    {
	    	if (card_id == -1)
	    	{
	    		break;
	    	}
	    	func(card_id);
		}
	}

	void for_each_device(int card_id, std::function<void(snd_ctl_t *, int)> func)
	{
		snd_ctl_t *ctl;
		std::string card_code = "hw:" + std::to_string(card_id);
		if (snd_ctl_open(&ctl, card_code.c_str(), SND_CTL_NONBLOCK) >= 0)
		{
			int device_id = -1;
			while(snd_ctl_rawmidi_next_device(ctl, &device_id) >= 0)
			{
		    	if (device_id == -1)
		    	{
		    		break;
		    	}
				func(ctl, device_id);
			}
			snd_ctl_close(ctl);
		}
	}

	void for_each_port(bool input, std::function<void(std::string, std::string)> func)
	{
		std::vector<std::pair<std::string, std::string>> codes_names;

		for_each_card([&](int card_id){

			for_each_device(card_id, [&](snd_ctl_t *ctl, int device_id){

			   	snd_rawmidi_info_t *info;
				snd_rawmidi_info_malloc(&info);
		    	snd_rawmidi_info_set_device(info, device_id);			 	
			 	snd_rawmidi_info_set_stream(info, input ? SND_RAWMIDI_STREAM_INPUT : SND_RAWMIDI_STREAM_OUTPUT);

			 	snd_ctl_rawmidi_info(ctl, info);
			 	int count = snd_rawmidi_info_get_subdevices_count(info);

			 	for(int sub_id = 0; sub_id < count; ++sub_id)
			 	{			 		
			 		snd_rawmidi_info_set_subdevice(info, sub_id);
				 	snd_ctl_rawmidi_info(ctl, info);

					std::string name = snd_rawmidi_info_get_subdevice_name(info);
				 	std::string code = "hw:" + std::to_string(card_id) + "," + std::to_string(device_id) + "," + std::to_string(sub_id);
				 	
				 	codes_names.push_back(std::make_pair(code, name));
			 	}

			 	snd_rawmidi_info_free(info);
			});
		});

 		for (auto code_name: codes_names)
 		{
	 		func(code_name.first, code_name.second);
	 	}

	}
}

//-------------------------------------------------------------------------------------------------

MIDI::MIDI()
{
    refresh_ports();
}

//-------------------------------------------------------------------------------------------------

MIDI::~MIDI()
{
    m_input_thread.stop();

	for (auto handle: m_output_ports)
	{
		snd_rawmidi_close(handle);
	}
}

//-------------------------------------------------------------------------------------------------

void MIDI::reset()
{
	m_receive_callback = nullptr;
    refresh_ports();
}

//-------------------------------------------------------------------------------------------------

void MIDI::refresh_ports()
{
    m_input_thread.reset();

	// inputs
	m_input_names.clear();
	for_each_port(true, [&](std::string code, std::string name){
		m_input_names.push_back(name);

        auto input_port = std::make_shared<MIDIInputPort>(code, name);
        m_input_thread.add_sender(input_port);
	});

	// ouputs
	m_output_names.clear();	
	for (auto handle: m_output_ports)
	{
		snd_rawmidi_close(handle);
	}
	m_output_ports.clear();
    
	for_each_port(false, [&](std::string code, std::string name){
		m_output_names.push_back(name);
		snd_rawmidi_t* handle = nullptr;
	    if (snd_rawmidi_open(NULL, &handle, code.c_str(), SND_RAWMIDI_SYNC) != 0)
	    {
	        throw std::runtime_error("Could not open MIDI output port " + name + " - " + code);
	    }
	    m_output_ports.push_back(handle);
	});

}

//-------------------------------------------------------------------------------------------------

void MIDI::on_frame()
{
    if (!m_input_thread.is_running())
    {
        m_input_thread.start();
    }

    m_input_thread.collect_and_dispatch();
}

//-------------------------------------------------------------------------------------------------

void MIDI::write_to_all_outputs(unsigned char* data, int size)
{
	for (auto handle: m_output_ports)
	{
		snd_rawmidi_write(handle, data, size);
	}
}

//-------------------------------------------------------------------------------------------------

void MIDI::send_channel_messsage(MIDIMessage::Type type, unsigned char channel, unsigned char byte1, unsigned char byte2)
{
    if (channel == 0 || channel > 16)
    {
        throw std::runtime_error("Channel number must be between 1 and 16");
    }

    unsigned char status = type + (channel - 1);

    switch (type)
    {
        case MIDIMessage::PROGRAM_CHANGE:
        case MIDIMessage::CHANNEL_PRESSURE:
        {
            unsigned char message[2]  = {status, byte1};
            write_to_all_outputs(message, 2);
            break;
        }
        case MIDIMessage::NOTE_ON:
        case MIDIMessage::NOTE_OFF:
        case MIDIMessage::AFTER_TOUCH:
        case MIDIMessage::CONTROL_CHANGE:
        case MIDIMessage::PITCH_BEND:
        {
            unsigned char message[3]  = {status, byte1, byte2};
            write_to_all_outputs(message, 3);
            break;
        }
        default:
            throw std::runtime_error("Invalid MIDI channel message");
        break;
    }
}

//-------------------------------------------------------------------------------------------------

void MIDI::send_system_messsage(MIDIMessage::Type type, unsigned char byte1, unsigned char byte2)
{
    switch (type)
    {
        case MIDIMessage::CLOCK:
        case MIDIMessage::START:
        case MIDIMessage::CONTINUE:
        case MIDIMessage::STOP:
        case MIDIMessage::SENSING:
        case MIDIMessage::RESET:
        case MIDIMessage::TUNE_REQUEST:
        {
            unsigned char status = type;
            write_to_all_outputs(&status, 1);
            break;
        }
        case MIDIMessage::TIMING_CODE:
        case MIDIMessage::SONG_SELECT:
        {
            unsigned char message[2]  = {type, byte1};
            write_to_all_outputs(message, 2);
            break;
        }
        case MIDIMessage::SONG_POSITION:
        {
            unsigned char message[3]  = {type, byte1, byte2};
            write_to_all_outputs(message, 3);
            break;
        }
        default:
            throw std::runtime_error("Invalid MIDI system message");
        break;
    }
}

//-------------------------------------------------------------------------------------------------

void MIDI::send_reset_all(unsigned char channel)
{
    send_channel_messsage(MIDIMessage::CONTROL_CHANGE, channel, 0x79);
}

//-------------------------------------------------------------------------------------------------

void MIDI::send_all_notes_off(unsigned char channel)
{
    send_channel_messsage(MIDIMessage::CONTROL_CHANGE, channel, 0x7B);
}

//-------------------------------------------------------------------------------------------------

void MIDI::on_bind(Lua& lua, Lua::Object& object) 
{
    Bindable::on_bind(lua, object);

    auto midi = lua.create_global("MIDI");

    midi.set("NOTE_ON", (int)MIDIMessage::NOTE_ON);
    midi.set("NOTE_OFF", (int)MIDIMessage::NOTE_OFF);
    midi.set("AFTER_TOUCH", (int)MIDIMessage::AFTER_TOUCH);
    midi.set("CONTROL_CHANGE", (int)MIDIMessage::CONTROL_CHANGE);
    midi.set("PROGRAM_CHANGE", (int)MIDIMessage::PROGRAM_CHANGE);
    midi.set("CHANNEL_PRESSURE", (int)MIDIMessage::CHANNEL_PRESSURE);
    midi.set("PITCH_BEND", (int)MIDIMessage::PITCH_BEND);

    midi.set("TIMING_CODE", (int)MIDIMessage::TIMING_CODE);
    midi.set("SONG_POSITION", (int)MIDIMessage::SONG_POSITION);
    midi.set("SONG_SELECT", (int)MIDIMessage::SONG_SELECT);
    midi.set("TUNE_REQUEST", (int)MIDIMessage::TUNE_REQUEST);
    midi.set("CLOCK", (int)MIDIMessage::CLOCK);
    midi.set("START", (int)MIDIMessage::START);
    midi.set("CONTINUE", (int)MIDIMessage::CONTINUE);
    midi.set("STOP", (int)MIDIMessage::STOP);
    midi.set("SENSING", (int)MIDIMessage::SENSING);
    midi.set("RESET", (int)MIDIMessage::RESET);

    m_input_thread.for_each_sender([&](std::shared_ptr<MIDIEventSender> sender){

        sender->set_events_callback([&](MIDIMessage& message){
            Lua::Object func = lua.get_global("onMIDIMessage");
            if (func.is<lua::Callable>())
            {
                func.call((int)message.type, message.data_1, message.data_2, message.channel);
            }
        });

    });

    object.set("send_channel_messsage", [&](int type, int channel, int byte1, int byte2) {
        send_channel_messsage((pi_kit::MIDIMessage::Type)type, channel == 0 ? 1 : channel, byte1, byte2);
    });
    object.set("send_system_messsage", [&](int type, int byte1, int byte2) {
        send_system_messsage((pi_kit::MIDIMessage::Type)type, byte1, byte2);
    });
    object.set("send_reset_all", [&](int channel) {
        send_reset_all(channel == 0 ? 1 : channel); 
    });
    object.set("send_all_notes_off", [&](int channel) {
        send_all_notes_off(channel == 0 ? 1 : channel); 
    });

}

//-------------------------------------------------------------------------------------------------

void MIDI::info()
{
    Log::title("Module", "MIDI");
    Log::info(description());
    Log::info("");
    Log::info("Channel Message Types:", "MIDI.NOTE_ON, MIDI.NOTE_OFF, MIDI.AFTER_TOUCH, MIDI.CONTROL_CHANGE, MIDI.PROGRAM_CHANGE, MIDI.CHANNEL_PRESSURE, MIDI.PITCH_BEND");
    Log::info("System Message Types:", "MIDI.TIMING_CODE, MIDI.SONG_POSITION, MIDI.SONG_SELECT, MIDI.TUNE_REQUEST, MIDI.CLOCK, MIDI.START, MIDI.CONTINUE, MIDI.STOP, MIDI.SENSING, MIDI.RESET");
    Log::info("");

    if (m_input_names.empty())
    {
	    Log::info("Available MIDI input ports:", "None");
    }
    else
    {
	    Log::info("Available MIDI input ports:");
		for (auto input : m_input_names)
		{
			Log::info(input.c_str());
		}
	}

    Log::info("");

    if (m_output_names.empty())
    {
	    Log::info("Available MIDI output ports:", "None");
    }
    else
    {
	    Log::info("Available MIDI output ports:");
		for (auto output : m_output_names)
		{
			Log::info(output.c_str());
		}
	}

    Log::info("");
    Log::info("Functions:");
    Log::func("send_channel_messsage", {"type", "channel", "data1", "data2"}, "Sends a MIDI channel message to all output ports. See above for a list of available message types. Channel is 1-based");
    Log::func("send_system_messsage", {"type", "data1", "data2"}, "Sends a MIDI system message to all output ports. See above for a list of available message types. SYSEX messages are not supported");
    Log::func("send_reset_all", {"channel"}, "Sends 'Reset All Controllers' CC message (0x79)");
    Log::func("send_all_notes_off", {"channel"}, "Sends 'All Notes Off' CC message (0x7B)");

    Log::info("");
    Log::info("Callbacks:");
    Log::func("onMIDIMessage", {}, "function onMIDIMessage(type, data1, data2, channel) will be called when receiving a MIDI message");
}

//-------------------------------------------------------------------------------------------------
}//pi_kit
