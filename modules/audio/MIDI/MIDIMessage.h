#pragma once

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

struct MIDIMessage
{
    static const int NONE = -1;
    static const int ALL = 0;

    enum Type
    {
        TYPE_NONE = 0x0,

        //channel messages
        NOTE_ON = 0x90,
        NOTE_OFF = 0x80,
        AFTER_TOUCH = 0xA0,
        CONTROL_CHANGE = 0xB0,
        PROGRAM_CHANGE = 0xC0,
        CHANNEL_PRESSURE = 0xD0,
        PITCH_BEND = 0xE0,

        //system messages
        TIMING_CODE = 0xF1,
        SONG_POSITION = 0xF2,
        SONG_SELECT = 0xF3,
        TUNE_REQUEST = 0xF6,
        CLOCK = 0xF8,
        START = 0xFA,
        CONTINUE = 0xFB,
        STOP = 0xFC,
        SENSING = 0xFE,
        RESET = 0xFF
    };

    Type type = TYPE_NONE;
    int channel = ALL;
    int data_1 = NONE;
    int data_2 = NONE;
};

//-------------------------------------------------------------------------------------------------
}//pi_kit
