#pragma once

#include "MIDIMessage.h"
#include <common/events.h>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

using MIDIEvent = Event<MIDIMessage>;
using MIDIEventSender = EventSender<MIDIMessage>;
using MIDIEventThread = EventThread<MIDIMessage>;

//-------------------------------------------------------------------------------------------------
}//pi_kit