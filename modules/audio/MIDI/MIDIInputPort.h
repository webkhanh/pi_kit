#pragma once

#include "events.h"
#include "MIDIMessage.h"
#include <alsa/asoundlib.h>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

class MIDIInputPort: public MIDIEventSender
{
public:

    MIDIInputPort(std::string code, std::string name);
    ~MIDIInputPort();

    std::shared_ptr<MIDIEvent> on_event_thread_update() override;

private:

    void read_status_byte(unsigned char byte);
    void read_data_byte(unsigned char byte);

    snd_rawmidi_t* m_handle = nullptr;

    static const int READY = 0;
    static const int UNKNOWN = -1;
    static const int SKIP = -2;
    int m_expected_bytes = UNKNOWN;
    MIDIMessage m_pending_message;
};

//-------------------------------------------------------------------------------------------------
}//pi_kit
