#include "MIDIInputPort.h"

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

MIDIInputPort::MIDIInputPort(std::string code, std::string name)
{
    if (snd_rawmidi_open(&m_handle, NULL, code.c_str(), SND_RAWMIDI_NONBLOCK) != 0)
    {
        throw std::runtime_error("Could not create MIDI input port " + name);
    }
}

//-------------------------------------------------------------------------------------------------

MIDIInputPort::~MIDIInputPort()
{
    if (m_handle)
    {
        snd_rawmidi_close(m_handle);
    }
}

//-------------------------------------------------------------------------------------------------

namespace {
    bool is_system_message(unsigned char byte)
    {
        return (byte & 0xF0) == 0xF0; // system message = 0xF?
    }
}

//-------------------------------------------------------------------------------------------------

void MIDIInputPort::read_status_byte(unsigned char byte)
{
    if (byte == 0xF0) //sysex start
    {
        m_expected_bytes = SKIP;
    }
    else if (byte == 0xF7) //sysex end
    {
        m_expected_bytes = UNKNOWN;
    }
    else
    {
        if (is_system_message(byte))
        {
            m_pending_message.type = (MIDIMessage::Type)byte;

            switch (m_pending_message.type)
            {
                case MIDIMessage::CLOCK:
                case MIDIMessage::START:
                case MIDIMessage::CONTINUE:
                case MIDIMessage::STOP:
                case MIDIMessage::SENSING:
                case MIDIMessage::RESET:
                case MIDIMessage::TUNE_REQUEST:
                {
                    m_expected_bytes = READY;
                    break;
                }
                case MIDIMessage::TIMING_CODE:
                case MIDIMessage::SONG_SELECT:
                {
                    m_expected_bytes = 1;
                    break;
                }
                case MIDIMessage::SONG_POSITION:
                {
                    m_expected_bytes = 2;
                    break;
                }
            }
        }
        else
        {
            m_pending_message.type = (MIDIMessage::Type)(byte & 0xF0);
            m_pending_message.channel = (byte & 0x0F) + 1; //channel is 1-based

            switch (m_pending_message.type)
            {
                case MIDIMessage::PROGRAM_CHANGE:
                case MIDIMessage::CHANNEL_PRESSURE:
                {
                    m_expected_bytes = 1;
                    break;
                }

                case MIDIMessage::NOTE_ON:
                case MIDIMessage::NOTE_OFF:
                case MIDIMessage::AFTER_TOUCH:
                case MIDIMessage::CONTROL_CHANGE:
                case MIDIMessage::PITCH_BEND:
                {
                    m_expected_bytes = 2;
                    break;
                }
            }
        }

        if (m_expected_bytes == UNKNOWN)
        {
            throw std::runtime_error("Invalid MIDI message");
        }
    }
}

//-------------------------------------------------------------------------------------------------

void MIDIInputPort::read_data_byte(unsigned char byte)
{
    if (m_pending_message.data_1 == MIDIMessage::NONE)
    {
        m_pending_message.data_1 = byte;
    }
    else
    {
        m_pending_message.data_2 = byte;
    }
    m_expected_bytes--;
}

//-------------------------------------------------------------------------------------------------

std::shared_ptr<MIDIEvent> MIDIInputPort::on_event_thread_update()
{
    unsigned char byte;
    bool pending_ready = false;

    if(snd_rawmidi_read(m_handle, &byte, 1) > 0)
    {
        if (m_expected_bytes == UNKNOWN)
        {
            m_pending_message = MIDIMessage();
            read_status_byte(byte);
        }
        else
        {
            read_data_byte(byte);
        }

        if (m_expected_bytes == READY)
        {
            pending_ready = true;
            m_expected_bytes = UNKNOWN;
        }
    }

    fflush(stdout);

    if(pending_ready)
    {
        return createEvent(m_pending_message);
    }
    return nullptr;
}

//-------------------------------------------------------------------------------------------------
}//pi_kit
