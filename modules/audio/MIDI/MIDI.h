#pragma once

#include "events.h"
#include "MIDIMessage.h"

#include <common/Module.h>
#include <apps/LuaApp/Bindable.h>

#include <alsa/asoundlib.h>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

class MIDI: public Module, public Bindable
{
public:
    
    static const char* description()
    {
        return "Creation & handling of MIDI Input + Output ports (MIDI - ALSA)";
    }

    MIDI();
    ~MIDI();

    void on_frame() override;
    void reset() override;
    void info() override;

    void send_channel_messsage(MIDIMessage::Type type, unsigned char channel, unsigned char byte1, unsigned char byte2 = 0);
    void send_system_messsage(MIDIMessage::Type type, unsigned char byte1 = 0, unsigned char byte2 = 0);

    void send_reset_all(unsigned char channel = 1);
    void send_all_notes_off(unsigned char channel = 1);
    
    using ReceiveCallback = std::function<void(MIDIMessage&)>;
    void set_receive_callback(ReceiveCallback callback)
    {
        m_receive_callback = callback;
    }

protected:

    void on_bind(Lua& lua, Lua::Object& object) override;

private:

    void refresh_ports();
    void write_to_all_outputs(unsigned char* data, int size);

    std::vector<std::string> m_input_names;
    std::vector<std::string> m_output_names;

    MIDIEventThread m_input_thread;
    std::vector<snd_rawmidi_t*> m_output_ports;

    ReceiveCallback m_receive_callback;
};

//-------------------------------------------------------------------------------------------------
}//pi_kit
