#include "Encoder.h"
#include <common/pins.h>
#include <wiringPi.h>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

Encoder::Encoder(int pin_a, int pin_b)
: m_pin_a(pin_a)
, m_pin_b(pin_b)
{
    int wiring_pin_a = pins::physical_to_WiringPi(m_pin_a);
    int wiring_pin_b = pins::physical_to_WiringPi(m_pin_b);
    
    if (!pins::is_input_capable(m_pin_a))
    {
        throw std::runtime_error("Pin #" + std::to_string(m_pin_a) + " is not a valid input pin");
    }
    if (!pins::is_input_capable(m_pin_b))
    {
        throw std::runtime_error("Pin #" + std::to_string(m_pin_b) + " is not a valid input pin");
    }

    pinMode(wiring_pin_a, INPUT);
    pinMode(wiring_pin_b, INPUT);
    pullUpDnControl(wiring_pin_a, PUD_UP);
    pullUpDnControl(wiring_pin_b, PUD_UP);

    m_previous_value = readValueFromPins();
}

//-------------------------------------------------------------------------------------------------

int Encoder::readValueFromPins()
{
    int MSB = digitalRead(pins::physical_to_WiringPi(m_pin_a));
    int LSB = digitalRead(pins::physical_to_WiringPi(m_pin_b));

    return (MSB << 1) | LSB;
}

//-------------------------------------------------------------------------------------------------

std::shared_ptr<HWEvent> Encoder::on_event_thread_update()
{
    int value = readValueFromPins();

    if (value == m_previous_value)
    {
        return nullptr;
    }

    int offset = 0;
    int sum = (m_previous_value << 2) | value;

    if (sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011)
    {
        offset = -1;
    }

    if(sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000)
    {
        offset = 1;
    }

    if (offset)
    {
        m_previous_value = value;

        m_accumulated_offset += offset;

        if ((m_accumulated_offset + (m_detent_size/2)) / m_detent_size != 
            (m_last_sent_offset + (m_detent_size/2)) / m_detent_size)
        {
            bool cw = m_accumulated_offset > m_last_sent_offset;
            m_last_sent_offset = m_accumulated_offset;

            HWEventData data;
            data.m_param = (int)(cw ? CW : CCW);
            data.m_id = m_pin_a;
            return createEvent(data);
        }
    }

    return nullptr;
}

//-------------------------------------------------------------------------------------------------
}//pi_kit
