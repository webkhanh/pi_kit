#pragma once

#include "Button.h"
#include "Encoder.h"
#include "InputPin.h"
#include "OutputPin.h"
#include "DistanceSensor.h"

#include "HWEventData.h"

#include <common/Module.h>
#include <apps/LuaApp/Bindable.h>

#include <memory>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

class GPIO: public Module, public Bindable
{
public:

    static const char* description()
    {
        return "Raspberry Pi's 40-pin GPIO access (GPIO - WiringPi, using physical pin numbers)";
    }

    GPIO();
    ~GPIO();

    void on_frame() override;
    void reset() override;
	
	void on_bind(Lua& lua, Lua::Object& object) override;
    void info() override;

	std::shared_ptr<Encoder> create_encoder(int pin_a, int pin_b);
    std::shared_ptr<InputPin> create_input_pin(int pin, InputPin::UpDownMode mode = InputPin::UP);
    std::shared_ptr<InputPin> create_output_pin(int pin, OutputPin::Mode mode = OutputPin::DIGITAL);
    std::shared_ptr<Button> create_button(int pin);
    std::shared_ptr<DistanceSensor> create_distance_sensor(int pin);

private:

	HWEventThread m_event_thread;
};

//-------------------------------------------------------------------------------------------------
}//pi_kit
