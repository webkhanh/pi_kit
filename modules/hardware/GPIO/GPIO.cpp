#include "GPIO.h"

#include <wiringPi.h>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

static bool s_instantiated = false;

//-------------------------------------------------------------------------------------------------

GPIO::GPIO()
{
    if (s_instantiated)
    {
        throw std::runtime_error("Only 1 GPIO module possible");
    }
    s_instantiated = true;

    wiringPiSetup();
}

//-------------------------------------------------------------------------------------------------

GPIO::~GPIO()
{
    m_event_thread.stop();
    s_instantiated = false;
}

//-------------------------------------------------------------------------------------------------

void GPIO::on_frame()
{
    if (!m_event_thread.is_running())
    {
        m_event_thread.start();
    }

    m_event_thread.collect_and_dispatch();
}

//-------------------------------------------------------------------------------------------------

void GPIO::reset()
{
    m_event_thread.reset();
}

//-------------------------------------------------------------------------------------------------

std::shared_ptr<Encoder> GPIO::create_encoder(int pin_a, int pin_b)
{
    auto encoder = std::make_shared<Encoder>(pin_a, pin_b);
    m_event_thread.add_sender(encoder);
    return encoder;
}

//-------------------------------------------------------------------------------------------------

std::shared_ptr<InputPin> GPIO::create_input_pin(int pin, InputPin::UpDownMode mode)
{
    auto input_pin = std::make_shared<InputPin>(pin, mode);
    m_event_thread.add_sender(input_pin);
    return input_pin;
}

//-------------------------------------------------------------------------------------------------

std::shared_ptr<Button> GPIO::create_button(int pin)
{
    auto button = std::make_shared<Button>(pin);
    m_event_thread.add_sender(button);
    return button;
}

//-------------------------------------------------------------------------------------------------

std::shared_ptr<DistanceSensor> GPIO::create_distance_sensor(int pin)
{
    auto sensor = std::make_shared<DistanceSensor>(pin);
    m_event_thread.add_sender(sensor);
    return sensor;
}

//-------------------------------------------------------------------------------------------------

void GPIO::on_bind(Lua& lua, Lua::Object& object)
{
    Bindable::on_bind(lua, object);

    auto pud = lua.create_global("INPUT");
    pud.set("UP", (int)InputPin::UP);
    pud.set("DOWN", (int)InputPin::DOWN);
    pud.set("NONE", (int)InputPin::NONE);    
    
    auto pwm = lua.create_global("OUTPUT");
    pwm.set("DIGITAL", (int)OutputPin::DIGITAL);
    pwm.set("PWM_SOFT", (int)OutputPin::PWM_SOFT);
    pwm.set("PWM_HARD", (int)OutputPin::PWM_HARD);

    object.set("create_button", [&](const int pin) -> Lua::Object
    {
        try
        {
            return lua.create_and_bind_object(create_button(pin));
        }
        catch(std::exception &e) {lua.throw_error(e.what()); return Lua::Object();}
    });

    object.set("create_input_pin", [&](const int pin, const int mode) -> Lua::Object
    {
        try
        {
            return lua.create_and_bind_object(create_input_pin(pin, (InputPin::UpDownMode)mode));
        }
        catch(std::exception &e) {lua.throw_error(e.what()); return Lua::Object();}
    });

    object.set("create_output_pin", [&](const int index, int mode)->Lua::Object
    {
        try
        {
            return lua.create_and_bind_object(std::make_shared<OutputPin>(index, (OutputPin::Mode)mode));
        }
        catch(std::exception &e) {lua.throw_error(e.what()); return Lua::Object();}
    });

    object.set("setupEncoder", [&](const int pin_a, const int pin_b, const int detent_size)
    {
        try
        {
            auto encoder = create_encoder(pin_a, pin_b);
            if (detent_size)
            {
                encoder->set_detents_size(detent_size);
            }

            encoder->set_events_callback([&](HWEventData& data){
                Lua::Object func = lua.get_global("on_encoder");
                if (func.is<lua::Callable>())
                {
                    func.call(data.m_id, data.m_param == Encoder::CW ? 1 : -1);
                }
            });
        }
        catch(std::exception &e) {lua.throw_error(e.what());}
    });

    object.set("create_ultrasonic", [&](const int pin)->Lua::Object
    {
        try
        {
            return lua.create_and_bind_object(create_distance_sensor(pin));
        }
        catch(std::exception &e) {lua.throw_error(e.what()); return Lua::Object();}
    });    
}

//-------------------------------------------------------------------------------------------------

void GPIO::info()
{
    Log::title("Module", "GPIO");
    Log::info(description());
    Log::GPIO();

    Log::info("");
    Log::info("Input Pin Pull Up/Down modes:", "INPUT.NONE, INPUT.UP, INPUT.DOWN");
    Log::info("Output Pin modes:", "OUTPUT.DIGITAL, OUTPUT.PWM_SOFT, OUTPUT.PWM_HARD (can only be used in sudo mode)");

    Log::info("");
    Log::info("Functions:");
    Log::func("create_input", {"pin", "mode (default: INPUT.UP)"}, "Creates and returns an Input Pin Object. function on_pin(pin, state) will be called when the pin state changes");
    Log::func("create_output", {"pin", "mode (default: OUTPUT.DIGITAL)"}, "Creates and returns an Output Pin Object");
    Log::func("create_button", {"pin"}, "Creates and returns a Button Object. function on_button_down(pin) and function on_button_up(pin) will be called when the button is pressed/released");
    Log::func("setup_encoder", {"pin A", "pin B", "pulses per detent (default: 1)"}, "Sets up an Encoder. function on_encoder(pin A, amount) will be called when the encoder is turned.");
    Log::func("create_ultrasonic", {"pin A", "pin B", "pulses per detent (default: 1)"}, "Creates an Encoder. function on_encoder(pin A, amount) will be called when the encoder is turned.");
    Log::info("Run .info() on objects for more info");
}

//-------------------------------------------------------------------------------------------------
}//pi_kit
