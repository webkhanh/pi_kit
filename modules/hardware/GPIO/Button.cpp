#include "Button.h"
#include <common/pins.h>
#include <wiringPi.h>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

Button::Button(int pin)
: m_pin(pin)
{
	if (!pins::is_input_capable(m_pin))
	{
		throw std::runtime_error("Pin #" + std::to_string(m_pin) + " is not a valid input pin");
	}

	int wiring_pin = pins::physical_to_WiringPi(m_pin);
    pinMode(wiring_pin, INPUT);
    pullUpDnControl(wiring_pin, PUD_UP);
    m_state = digitalRead(wiring_pin) == 0;
}

//-------------------------------------------------------------------------------------------------

std::shared_ptr<HWEvent> Button::on_event_thread_update()
{
	const bool state = digitalRead(pins::physical_to_WiringPi(m_pin)) == 0;
	if (state != m_state)
	{
		size_t clock_now = clock();
		if (clock_now - m_last_change > m_debounce_time)
		{
			m_state = state;
			m_last_change = clock_now;

			HWEventData data;
			data.m_param = (int)(m_state ? BUTTON_PRESSED : BUTTON_RELEASED);
			data.m_id = m_pin;
		    return createEvent(data);
		}
	}

	return nullptr;
}

//-------------------------------------------------------------------------------------------------

bool Button::is_pressed() const 
{
    return m_state;
}

//-------------------------------------------------------------------------------------------------

void Button::on_bind(Lua& lua, Lua::Object& object)
{
	Bindable::on_bind(lua, object);
    object.set("is_pressed", [&]()->bool {return is_pressed();});
    object.set("set_debounce_time", [&](float time) {set_debounce_time(time);});

	set_events_callback([&](HWEventData& data){
    	Lua::Object func = lua.get_global(data.m_param == BUTTON_PRESSED ? "onButtonPressed" : "onButtonReleased");
    	if (func.is<lua::Callable>())
    	{
    		func.call(data.m_id);
    	}
	});
}

//-------------------------------------------------------------------------------------------------

void Button::info()
{
    Log::title("Object", "Momentary Button");
    Log::info("");
    Log::info("Defaults:");
    Log::info("Debounce time", "0.2 seconds (50 ms) - time for the button to stabilise itself after a press");
    Log::info("");
    Log::info("Functions:");
    Log::func("is_down", {}, "Returns whether button is pressed or not");
    Log::func("set_debounce_time", {"time (seconds)"}, "Sets the button's debounce time");
    Log::info("");
    Log::info("Callbacks:");
    Log::func("on_button_down", {"pin"}, "Called when button is pressed");
    Log::func("on_button_up", {"pin"}, "Called when button is released");
}

//-------------------------------------------------------------------------------------------------
}//pi_kit
