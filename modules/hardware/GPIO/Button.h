#pragma once

#include "HWEventData.h"

#include <apps/LuaApp/Bindable.h>

namespace pi_kit {

class InputPin;

//-------------------------------------------------------------------------------------------------

class Button: public Bindable, public HWEventSender
{

public:

	enum EventType
	{
		BUTTON_PRESSED,
		BUTTON_RELEASED
	};

    Button(int pin);
    
    bool is_pressed() const;

    void set_debounce_time(float seconds)
    {
    	m_debounce_time = CLOCKS_PER_SEC * seconds;
    }

    void on_bind(Lua& lua, Lua::Object& object) override;
    void info() override;

    std::shared_ptr<HWEvent> on_event_thread_update() override;

private:

    int m_pin;
    bool m_state;
    size_t m_last_change = 0;
    float m_debounce_time = CLOCKS_PER_SEC * 0.2f;
};

//-------------------------------------------------------------------------------------------------
}//pi_kit
