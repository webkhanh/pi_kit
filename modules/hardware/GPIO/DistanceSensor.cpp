#include "DistanceSensor.h"
#include <common/pins.h>
#include <wiringPi.h>
#include <unistd.h>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

DistanceSensor::DistanceSensor(int pin)
: m_pin(pin)
{
    if (!pins::is_input_capable(pin)|| !pins::is_output_capable(pin))
    {
        throw std::runtime_error("Pin #" + std::to_string(pin) + " is not a valid sensor pin (must be digital input/output-capable)");
    }
}

//-------------------------------------------------------------------------------------------------

std::shared_ptr<HWEvent> DistanceSensor::on_event_thread_update()
{
    int pin = pins::physical_to_WiringPi(m_pin);

    auto next_state = [&](){
        m_clock = clock();
        switch(m_state)
        {
            case REQUEST_READ: m_state = EMIT_PENDING; break;
            case EMIT_PENDING: m_state = EMITTING; break;
            case EMITTING: m_state = WAIT_AND_SEND; break;
            case WAIT_AND_SEND: m_state = REQUEST_READ; break;
        }
    };

    auto reset_state = [&](){
        m_clock = clock();
        m_state = WAIT_AND_SEND;
    };

    size_t elapsed = clock() - m_clock;

    switch (m_state)
    {
        case REQUEST_READ:
        {
            pinMode(pin, OUTPUT);
            digitalWrite(pin, 0);
            usleep(2);
            digitalWrite(pin, 1);
            usleep(5);
            digitalWrite(pin, 0);

            pinMode(pin, INPUT);
            pullUpDnControl(pin, PUD_OFF);
            next_state();
            break;
        }
        case EMIT_PENDING:
        {
            if (digitalRead(pin) == 1)
            {
                next_state();
            }
            else if (elapsed > 100000) //time out
            {
                reset_state();
            }
            break;
        }
        case EMITTING:
        {
            if (digitalRead(pin) == 0)
            {
                const float seconds = float(elapsed) / float(CLOCKS_PER_SEC);
                m_distance = seconds * 34000.f / 2.f;
                next_state();
            }
            else if (elapsed > 1000000) //time out
            {
                reset_state();
            }
            break;
        }
        case WAIT_AND_SEND:
        {
            if (elapsed > 200000)
            {
                next_state();
            }
        }
    }

    return nullptr;
}

//-------------------------------------------------------------------------------------------------

float DistanceSensor::get_distance()
{
    return m_distance;
}

//-------------------------------------------------------------------------------------------------

void DistanceSensor::on_bind(Lua& lua, Lua::Object& object)
{
	Bindable::on_bind(lua, object);
    object.set("get_distance", [&]() -> float {return get_distance();} );
}

//-------------------------------------------------------------------------------------------------

void DistanceSensor::info()
{
    Log::title("Object", "Distance Sensor.");
    Log::info("Reads distance in cm about 5 times a second");
    Log::info("");
    Log::info("Functions:");
    Log::func("get_distance", {}, "Returns latest distance reading in cm");
}

//-------------------------------------------------------------------------------------------------
}//pi_kit
