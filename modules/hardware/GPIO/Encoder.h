#pragma once

#include "HWEventData.h"

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

class Encoder: public HWEventSender
{

public:

	enum EventType
	{
		CW,
		CCW
	};

    Encoder(int pin_a, int pin_b);

    std::shared_ptr<HWEvent> on_event_thread_update() override;

    void set_detents_size(int size)
    {
    	m_detent_size = size;
    }

private:

	int readValueFromPins();

    int m_pin_a;
    int m_pin_b;

    int m_previous_value = 0;

    int m_last_sent_offset = 0;
    int m_accumulated_offset = 0;

    int m_detent_size = 1;
};

//-------------------------------------------------------------------------------------------------
}//pi_kit
