#pragma once

#include <apps/LuaApp/Bindable.h>

#include <memory>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

class OutputPin: public Bindable
{
public:

	enum Mode
	{
		DIGITAL = 0,
		PWM_SOFT,
		PWM_HARD
	};

    OutputPin(int index, Mode mode = DIGITAL);

    void write(float value);

    void on_bind(Lua& lua, Lua::Object& value) override;
    void info() override;

private:

    int m_index;
    Mode m_mode;

};

//-------------------------------------------------------------------------------------------------
}//pi_kit
