#pragma once

#include "HWEventData.h"

#include <apps/LuaApp/Bindable.h>

#include <memory>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

class InputPin: public Bindable, public HWEventSender
{
public:

	enum UpDownMode
	{
		UP = 0,
		DOWN,
		NONE
	};

	enum EventType
	{
		PIN_HIGH,
		PIN_LOW
	};

    InputPin(int index, UpDownMode mode = UP);

    bool get_state() const {return m_state;}

    std::shared_ptr<HWEvent> on_event_thread_update() override;

    void on_bind(Lua& lua, Lua::Object& value) override;
    void info() override;

private:

    int m_pin;
    bool m_state;

};

//-------------------------------------------------------------------------------------------------
}//pi_kit
