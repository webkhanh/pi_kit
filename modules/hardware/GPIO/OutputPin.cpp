#include "OutputPin.h"
#include <common/pins.h>
#include <wiringPi.h>
#include <softPwm.h>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

OutputPin::OutputPin(int index, Mode mode)
: m_index(pins::physical_to_WiringPi(index))
, m_mode(mode)
{
	if (mode == DIGITAL || PWM_SOFT)
	{
	    if (!pins::is_output_capable(index))
		{
		    throw std::runtime_error("Pin #" + std::to_string(index) + " is not a valid output pin");
		}		
	}

    switch (mode)
    {
    	case DIGITAL:
    	pinMode(m_index, OUTPUT);
    	break;

    	case PWM_SOFT:
 		softPwmCreate (m_index, 0, 100);
    	break;

    	case PWM_HARD:
	    if (!pins::is_hw_pwm_capable(index))
		{
		    throw std::runtime_error("Pin #" + std::to_string(index) + " is not a valid hardware PWM pin");
		}

    	pinMode(m_index, PWM_OUTPUT);
    	break;

    	default:
    	throw std::runtime_error("Invalid output pin mode");
    	break;
    }
}

//-------------------------------------------------------------------------------------------------

void OutputPin::write(float value)
{
    switch (m_mode)
    {
    	case DIGITAL:
    	digitalWrite(m_index, (int)value);
    	break;

    	case PWM_SOFT:
 		softPwmWrite(m_index, (int)(value * 1024.f));
    	break;

    	case PWM_HARD:
	    pwmWrite(m_index, (int)(value * 100.f));
    	break;
    }
}

//-------------------------------------------------------------------------------------------------

void OutputPin::on_bind(Lua& lua, Lua::Object& object)
{
	Bindable::on_bind(lua, object);
    object.set("write", [&](float value) {write(value);} );
}

//-------------------------------------------------------------------------------------------------

void OutputPin::info()
{
    Log::title("Object", "Output Pin");
    Log::info("");
    Log::info("Functions:");
    Log::func("write", {"value"}, "Write pin state: 0 or 1 for OUTPUT.DIGITAL pins, [0..1] otherwise");
}

//-------------------------------------------------------------------------------------------------
}//pi_kit
