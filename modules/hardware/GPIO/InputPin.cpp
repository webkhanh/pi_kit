#include "InputPin.h"
#include <common/pins.h>
#include <wiringPi.h>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

InputPin::InputPin(int pin, UpDownMode mode)
: m_pin(pin)
{
    if (!pins::is_input_capable(pin))
    {
        throw std::runtime_error("Pin #" + std::to_string(pin) + " is not a valid input pin");
    }

	int wiring_pin = pins::physical_to_WiringPi(m_pin);

    pinMode(wiring_pin, INPUT);

    if (mode == UP || mode == DOWN)
    {
    	pullUpDnControl(wiring_pin, mode == UP ? PUD_UP : PUD_DOWN);
	}

    m_state = digitalRead(wiring_pin) == 0;
}

//-------------------------------------------------------------------------------------------------

std::shared_ptr<HWEvent> InputPin::on_event_thread_update()
{
	const bool state = digitalRead(pins::physical_to_WiringPi(m_pin)) == 0;
	if (state != m_state)
	{
	    m_state = state;

        HWEventData data;
        data.m_param = m_state ? PIN_HIGH : PIN_LOW;
        data.m_id = m_pin;
        return createEvent(data);
	}
    
    return nullptr;
}

//-------------------------------------------------------------------------------------------------

void InputPin::on_bind(Lua& lua, Lua::Object& object)
{
	Bindable::on_bind(lua, object);
    object.set("get_state", [&]()->bool {return get_state();} );

    set_events_callback([&](HWEventData& data){
        Lua::Object func = lua.get_global("on_pin");
        if (func.is<lua::Callable>())
        {
            func.call(data.m_id, data.m_param == PIN_HIGH);
        }
    });
}

//-------------------------------------------------------------------------------------------------

void InputPin::info()
{
    Log::title("Object", "Input Pin");
    Log::info("");
    Log::info("Functions:");
    Log::func("get_state", {}, "Reads pin high/low state (true/false)");
    Log::info("");
    Log::info("Callbacks:");
    Log::func("on_pin", {"pin"}, "Called when pin state changes");
}

//-------------------------------------------------------------------------------------------------
}//pi_kit
