#pragma once

#include "HWEventData.h"

#include <apps/LuaApp/Bindable.h>

#include <memory>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

class DistanceSensor: public Bindable, public HWEventSender
{
public:

    DistanceSensor(int pin);

    std::shared_ptr<HWEvent> on_event_thread_update() override;

    void on_bind(Lua& lua, Lua::Object& value) override;
    void info() override;

    float get_distance();

private:

    int m_pin;
    float m_distance = 0;

    enum State
    {
    	REQUEST_READ = 0,
    	EMIT_PENDING,
    	EMITTING,
    	WAIT_AND_SEND
    };

    State m_state = REQUEST_READ;
    size_t m_clock = 0;
};

//-------------------------------------------------------------------------------------------------
}//pi_kit
