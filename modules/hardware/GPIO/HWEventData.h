#pragma once

#include <common/events.h>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

struct HWEventData
{
	static const int UNSPECIFIED = -1;
	int m_id = UNSPECIFIED;
	int m_param = UNSPECIFIED;
};

//-------------------------------------------------------------------------------------------------

using HWEvent = Event<HWEventData>;
using HWEventSender = EventSender<HWEventData>;
using HWEventThread = EventThread<HWEventData>;

//-------------------------------------------------------------------------------------------------
}//pi_kit