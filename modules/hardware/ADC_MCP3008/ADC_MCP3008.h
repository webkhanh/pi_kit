#pragma once

#include <common/Module.h>
#include <apps/LuaApp/Bindable.h>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

class ADC_MCP3008 : public Module, public Bindable
{
public:

    static const char* description()
    {
        return "Analog-to-digital component driver module (ADC_MCP3008 - MCP3004/MCP3008 chips)\n";
    }
	    
    ADC_MCP3008(int spi_channel = 0);

    float read(int channel);
    void set_spi(int channel);

    void info() override;

protected:

    void on_bind(Lua& lua, Lua::Object&) override;

private:

	int m_pin_start = 100;
	int m_num_channels = 8;
    int m_spi_channel = 0;
};

//-------------------------------------------------------------------------------------------------
}//pi_kit
