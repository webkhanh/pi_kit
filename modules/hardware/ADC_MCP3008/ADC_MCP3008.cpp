#include "ADC_MCP3008.h"

#include <mcp3004.h>
#include <wiringPi.h>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

ADC_MCP3008::ADC_MCP3008(int spi_channel)
: m_spi_channel(spi_channel)
, m_pin_start(spi_channel == 0 ? 100 : 200)
{
	mcp3004Setup(m_pin_start , m_spi_channel);
}

//-------------------------------------------------------------------------------------------------

void ADC_MCP3008::set_spi(int channel)
{
    if (channel != 0 && channel != 1)
    {
        throw std::runtime_error("Invalid SPI channel (0 or 1 -> CE0 or CE1)");
    }

    m_spi_channel = channel;
    m_pin_start = m_spi_channel == 0 ? 100 : 200;
    mcp3004Setup(m_pin_start, m_spi_channel);
}

//-------------------------------------------------------------------------------------------------

float ADC_MCP3008::read(int channel)
{
    if (channel < 0 || channel >= m_num_channels)
    {
        throw std::runtime_error("Invalid Channel");
    }
    return static_cast<float>(analogRead(m_pin_start + channel)) / 1023.f;    	
}

//-------------------------------------------------------------------------------------------------

void ADC_MCP3008::on_bind(Lua& lua, Lua::Object& object)
{
    Bindable::on_bind(lua, object);

    object.set("read", [&](const int channel)->float
    {
        try
        {
            return read(channel);
        }
        catch(std::exception &e) {lua.throw_error(e.what()); return 0;}
    });    	
}

//-------------------------------------------------------------------------------------------------

void ADC_MCP3008::info()
{
    Log::title("Module", "ADC_MCP3008");
    Log::info(description());
    Log::info("SPI0 connections", "VCC: +3V3, DIN->MOSI: 19, DOUT->MISO: 21, SCL/SCLK: 23, CS: 24(CE0) or 26(CE1)");
    Log::info("SPI Channel", m_spi_channel == 0 ? "CE0" : "CE1");
    Log::info("");
    Log::info("Functions:");
    Log::func("read", {"channel"}, "Returns value (0..1) from an input channel (1..8)");
    Log::func("set_spi", {"channel (0 or 1)"}, "Sets SPI channel to use for the ADC (CE0/CE1)");
}

//-------------------------------------------------------------------------------------------------
}//pi_kit
