#pragma once

#include "Lua.h"

#include <common/Log.h>

#include <string>
#include <iostream>

namespace pi_kit {

class Bindable
{
public:

	virtual void info()
	{
		Log::info("No information available");
	}

protected:

    virtual void on_bind(class Lua& lua, Lua::Object& object)
    {
        object.set("info", [&]() {info();});
    }

    friend Lua;
};



}//pi_kit
