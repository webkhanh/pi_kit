#include "Lua.h"

#include "Bindable.h"

#include <sys/stat.h>
#include <iostream> //cout

namespace pi_kit {

//-------------------------------------------------------------------------------------------------
    
namespace
{
    time_t get_file_last_modified_date(const char *file)
    {
        struct tm *clock;
        struct stat attr;

        return stat(file, &attr) == 0 ? attr.st_mtime : 0;
    }
}

//-------------------------------------------------------------------------------------------------

Lua::Lua()
{
    m_state.reset(new lua::State());
}

//-------------------------------------------------------------------------------------------------

void Lua::create_and_bind_global(Bindable& bindable, std::string name)
{
    auto object = create_global(name.c_str());
    bindable.on_bind(*this, object);
}

//-------------------------------------------------------------------------------------------------

Lua::Object Lua::create_and_bind_object(std::shared_ptr<Bindable> p)
{
    Object table = m_state->newTable();
    table.set("get", [p]() -> lua::Pointer {
        return p.get();
    });
    p->on_bind(*this, table);
    return table;
}

//-------------------------------------------------------------------------------------------------

void Lua::run_string(const std::string& string)
{
    m_state->doString(string.c_str());
}

//-------------------------------------------------------------------------------------------------

Lua::Exists Lua::run_file(const std::string& file_path)
{
    time_t file_time = get_file_last_modified_date(file_path.c_str());
    if (file_time == 0)
    {
        return false;
    }

    m_script_files[file_path] = file_time;
    m_state->doFile(file_path);
    return true;
}

//-------------------------------------------------------------------------------------------------

Lua::Reload Lua::check_auto_reload()
{
    for (auto it = m_script_files.begin(); it != m_script_files.end(); ++it)
    {
        const auto &file = *it;
        auto name = file.first;
        auto time = file.second;
        auto newTime = get_file_last_modified_date(name.c_str());

        if (newTime == 0)
        {
            m_script_files.erase(it);
        }
        else if (newTime && newTime != time)
        {
            return true;
        }
    }
    return false;
}

//-------------------------------------------------------------------------------------------------
}//pi_kit
