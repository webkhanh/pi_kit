#include "LuaApp.h"

#include "Bindable.h"

#include <unistd.h> //sleep
#include <iostream> //cout
#include <signal.h>
#include <arpa/inet.h> //sockaddr_in
#include <ifaddrs.h> //getifaddrs

namespace pi_kit {

namespace {
    void listIPs()
    {
        ifaddrs* addrs;
        ifaddrs* tmp;
        getifaddrs(&addrs);
        tmp = addrs;
        int count = 0;

        Log::info("IPs:");

        while (tmp) 
        {
            if (tmp->ifa_addr && tmp->ifa_addr->sa_family == AF_INET && strcmp(tmp->ifa_name, "lo") != 0)
            {
                struct sockaddr_in *pAddr = (struct sockaddr_in *)tmp->ifa_addr;
                Log::info(tmp->ifa_name, inet_ntoa(pAddr->sin_addr));
                count++;
            }

            tmp = tmp->ifa_next;
        }

        if (count == 0)
        {
            Log::info("Not connected");
        }

        freeifaddrs(addrs);
    }
}

static bool s_must_exit = false;
void on_exit(int code)
{
    s_must_exit = true;
}

static bool s_instantiated = false;

//-------------------------------------------------------------------------------------------------

LuaApp::LuaApp()
{
    if (s_instantiated)
    {
        throw std::runtime_error("Only 1 app possible");
    }
    s_instantiated = true;

    system("clear");
    Log::app("The Maker's Framework - Loader", "(CTRL+C to exit)");
    Log::info("Run info() for more info");
    listIPs();

    signal(SIGINT, on_exit);
}

//-------------------------------------------------------------------------------------------------

LuaApp::~LuaApp()
{
    std::cout << std::endl;

    Log::app("Exiting");

    m_app.for_each_module([&](std::shared_ptr<Module> module){
        if (auto bindable = std::dynamic_pointer_cast<Bindable>(module))
        {
            std::string name = m_module_names[module.get()];
            if (!name.empty())
            {
                Log::module(name.c_str());
            }
        }
    });

    std::cout << std::endl;

    if (m_auto_reload_check_thread)
    {
        m_stop_auto_reload_thread = true;
        m_auto_reload_check_thread->join();
    }

    s_instantiated = false;
}

//-------------------------------------------------------------------------------------------------

void LuaApp::bind_app()
{
    m_lua->set_global("on_init", [](){});
    m_lua->set_global("on_loop", [](){});
    m_lua->set_global("on_exit", [](){});

    m_lua->set_global("run", [&](const char * file_path){
        try
        {
            m_lua->run_file(file_path ? file_path : "");
        }
        catch(std::exception &e)
        {
            error(e.what());
        }        
    });

    m_lua->set_global("exit", [&](){
        s_must_exit = true;
    });

    m_lua->set_global("reset", [&](){
        m_must_reset = true;
    });

    m_lua->set_global("time", [&]() -> float {
        return time();
    });

    m_lua->set_global("elapsed", [&]() -> float {
        return elapsed();
    });

    m_lua->set_global("info", [&]() {
        Log::info("Khanh Tech Course Programming Framework");
        Log::info("For more information on the LUA scripting language please visit www.lua.org");
        Log::info("");
        listIPs();

        std::string modules = "";
        m_app.for_each_module([&](std::shared_ptr<Module> module){
            if (auto bindable = std::dynamic_pointer_cast<Bindable>(module))
            {
                std::string name = m_module_names[module.get()];
                if (!name.empty())
                {
                    if (!modules.empty())
                    {
                        modules += ", ";
                    }
                    modules += name;
                }
            }
        });

        Log::info("");
        Log::info("Loaded modules:", modules.c_str());
        Log::info("Run .info() on modules for more info");

        Log::info("");
        Log::info("Framework functions:");
        Log::func("run", {"file"}, "Loads and runs a script file. App will reset automatically when the file is modified");
        Log::func("exit", {}, "Exits");
        Log::func("reset", {}, "Manual reset");
        Log::func("time", {}, "Number of seconds elapsed since start / last reset");
        Log::func("elapsed", {}, "Number of seconds elapsed since last frame");

        Log::info("");
        Log::info("Callbacks:");
        Log::func("on_init", {}, "Called when booting/rebooting");
        Log::func("on_loop", {}, "Called periodically (60 times a second)");
        Log::func("on_exit", {}, "Called when resetting/exiting");
    });
}

//-------------------------------------------------------------------------------------------------

void LuaApp::reset(bool reset_modules)
{
    try
    {
        if (m_lua)
        {
            m_lua->run_string(std::string("on_exit()"));
        }
    }
    catch(std::exception &e)
    {
        error(e.what());
    }

    m_lua.reset(new Lua());
    m_lua_error = false;

    bind_app();

    m_app.for_each_module([&](std::shared_ptr<Module> module){
        if (reset_modules)
        {
            module->reset();
        }

        if (auto bindable = std::dynamic_pointer_cast<Bindable>(module))
        {
            std::string name = m_module_names[module.get()];
            if (!name.empty())
            {
                m_lua->create_and_bind_global(*bindable.get(), name);
            }
        }
    });

    if (m_reset_func)
    {
        m_reset_func();
    }

    m_last_frame_clocks = clock();
    m_total_seconds = 0;

    try
    {
        Log::info("");
        Log::date();
        Log::app("Running", m_main_file.c_str());
        if (!m_lua->run_file(m_main_file))
        {
            error(std::string("Could not run file " + m_main_file).c_str());
            s_must_exit = true;
            return;
        }
        m_lua->run_string(std::string("on_init()"));
    }
    catch(std::exception &e)
    {
        error(e.what());
    }
}

//-------------------------------------------------------------------------------------------------

void LuaApp::error(const char* message)
{
    Log::error(message);

    m_app.for_each_module([](std::shared_ptr<Module> module){
        module->on_error();
    });

    Log::app("Stopped");

    m_lua_error = true;
}

//-------------------------------------------------------------------------------------------------

Running LuaApp::update()
{
    const bool first_run = m_lua == nullptr;
    if (first_run || m_must_reset)
    {
        m_must_reset = false;

        try
        {
            reset(!first_run);
        }
        catch(std::exception &e)
        {
            error(e.what());
            return false;
        }    
    }

    size_t clock_now = clock();

    if (!m_lua_error)
    {
        size_t elapsed = clock_now - m_last_frame_clocks;
        if (elapsed >= CLOCKS_PER_SEC / m_frame_rate)
        {
            m_last_frame_clocks = clock_now;
            m_seconds_since_last_frame = float(elapsed) / float(CLOCKS_PER_SEC);
            m_total_seconds += m_seconds_since_last_frame;

            try
            {
                m_app.for_each_module([](std::shared_ptr<Module> module){
                    module->on_frame();
                });

                if (m_update_func)
                {
                    s_must_exit |= m_update_func();
                }

                m_lua->run_string(std::string("on_frame()"));
            }
            catch(std::exception &e)
            {
                error(e.what());
            }         
        }
    }

    if (!m_auto_reload_check_thread)
    {
        m_auto_reload_check_thread.reset(new std::thread([&](){
            while(!m_stop_auto_reload_thread)
            {
                if (m_lua->check_auto_reload())
                {
                    sleep(1); // file cache needs time
                    m_must_reset = true;
                }
                sleep(1);
            }
        }));
    }

    return !s_must_exit;
}

//-------------------------------------------------------------------------------------------------
}//pi_kit
