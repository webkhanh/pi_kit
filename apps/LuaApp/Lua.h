#pragma once

#include <third_party/LuaState/include/LuaState.h>
#include <map>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

class Lua
{
public:

    using Reload = bool;
    using Exists = bool;
    using Object = lua::Value;

    Lua();

    Object create_global(const char* name) const
    {
        m_state->set(name, lua::Table());
        return (*m_state)[name];
    }

    void create_and_bind_global(class Bindable& bindable, std::string name);

    Object create_and_bind_object(std::shared_ptr<Bindable> p);

    template <class T>
    T* unwrap(Object object) const
    {
        void* ptr = object["get"]().to<lua::Pointer>();
        return dynamic_cast<T*>(ptr);
    }

    template<class T>
    void set_global(const char* key, T value)
    {
        m_state->set<T>(key, value);
    }
    
    Object get_global(const char* key)
    {
        return m_state->operator[](key);
    }

    void run_string(const std::string& string);
    Exists run_file(const std::string& file_path);

    void throw_error(const char* what) {luaL_error(m_state->getState(), what);}

    Reload check_auto_reload();

private:

    std::unique_ptr<lua::State> m_state;    
    std::map<std::string, time_t> m_script_files;

};

//-------------------------------------------------------------------------------------------------
}//pi_kit
