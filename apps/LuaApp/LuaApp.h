#pragma once

#include "Lua.h"

#include <common/App.h>
#include <common/Log.h>

#include <thread>
#include <vector>

namespace pi_kit {

using Running = bool;

//-------------------------------------------------------------------------------------------------

class LuaApp
{

public:

    LuaApp();
    ~LuaApp();

    template <class T>
    std::shared_ptr<T> create_module(std::string name)
    {
        Log::module(name.c_str(), T::description());
        
        try
        {
            auto module = std::shared_ptr<T>(new T());
            m_app.add_module(module);
            m_module_names[module.get()] = name;
            return module;
        }
        catch(std::exception &e)
        {
            Log::error(e.what());
            Log::error("Module not loaded");
            return nullptr;
        }
    }

    template <class T>
    void add_module(std::shared_ptr<T> module, std::string name)
    {
        Log::module(name.c_str(), T::description());
        m_app.add_module(module);
        m_module_names[module.get()] = name;
    }

    void set_frame_rate(int rate) {m_frame_rate = rate;}

    void on_reset(std::function<void()> func)
    {
        m_reset_func = func;
    }

    void on_update(std::function<Running()> func)
    {
        m_update_func = func;
    }

    void run(std::string name = "script.lua")
    {
        m_main_file = name;
        while(update());
    }

    float time() const
    {
        return m_total_seconds;
    }

    float elapsed() const
    {
        return m_seconds_since_last_frame;
    }

private:

    void bind_app();
    void reset(bool reset_modules);
    Running update();
    void error(const char* message);

    std::string m_main_file = "script.lua";

    std::function<Running()> m_update_func;
    std::function<void()> m_reset_func;

    int m_frame_rate = 60;

    bool m_lua_error = false;
    bool m_must_reset = true;
    
    size_t m_last_frame_clocks = 0;
    float m_total_seconds = 0;
    float m_seconds_since_last_frame = 0;

    App m_app;
    std::unique_ptr<Lua> m_lua;
    std::map<Module*, std::string> m_module_names;
    std::unique_ptr<std::thread> m_auto_reload_check_thread;
    bool m_stop_auto_reload_thread = false;
};

//-------------------------------------------------------------------------------------------------
}//pi_kit
