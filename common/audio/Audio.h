#pragma once

#include <PortAudio/portaudio.h>
#include <functional>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

class Audio
{
public:

	using Callback = std::function<void(const void* in, void* out, int size)>;
    static const int DEFAULT = -1;

    Audio();
    ~Audio();

    void start(int bit_depth = 16, int rate = 44100, int device_id = DEFAULT);
    void stop();

    void set_events_callback(Callback callback)
    {
    	m_callback = callback;
    }

    Callback get_callback() const
    {
    	return m_callback;
    }

    int get_frame_size() const
    {
        return m_frame_size;
    }

    int get_sample_rate() const
    {
        return m_sample_rate;
    }

    void list_devices() const;

private:

    int m_frame_size = 0;
    int m_sample_rate = 44100;

    PaStream* m_stream;
    Callback m_callback;

};

//-------------------------------------------------------------------------------------------------
}//pi_kit
