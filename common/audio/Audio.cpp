#include "Audio.h"

#include <common/Log.h>

#include <iostream> //cout

namespace pi_kit {

static int pa_callback(const void* in, void* out,
                        unsigned long num_frames,
                        const PaStreamCallbackTimeInfo* time_info,
                        PaStreamCallbackFlags status_flags,
                        void *user_data)
{
    Audio* self = static_cast<Audio*>(user_data);
    int frame_size = self->get_frame_size();

    for (int i=0; i < num_frames; i++)
    {
        if (auto callback = self->get_callback())
        {
            callback(in, out, frame_size);
        }
        char* _out = (char*)out;
        _out += frame_size;
    }

    return 0;
}

static bool s_instantiated = false;

//-------------------------------------------------------------------------------------------------

Audio::Audio()
{
    if (s_instantiated)
    {
        throw std::runtime_error("Only 1 audio module possible");
    }

    if (Pa_Initialize() != paNoError || Pa_GetDefaultOutputDevice() == -1)
    {
        throw std::runtime_error("Could not initialize Audio");
    }

    s_instantiated = true;
}

//-------------------------------------------------------------------------------------------------

Audio::~Audio()
{
    if (Pa_CloseStream(m_stream) != paNoError)
    {
        std::cout << "Warning: Could not close stream" << std::endl;
    }
    
    if (Pa_Terminate() != paNoError)
    {
        std::cout << "Warning: Could not terminate Audio" << std::endl;
    }

    s_instantiated = false;
}

//-------------------------------------------------------------------------------------------------

void Audio::list_devices() const
{
    int num_devices = Pa_GetDeviceCount();
    int default_input = Pa_GetDefaultInputDevice();
    int default_output = Pa_GetDefaultOutputDevice();

    for (int i = 0; i < num_devices; ++i)
    {
        const PaDeviceInfo* device_info = Pa_GetDeviceInfo(i);
        int num_inputs = device_info->maxInputChannels;
        int num_outputs = device_info->maxOutputChannels;

        std::string default_io;
        if (default_input == default_output && i == default_input)
        {
            default_io = " (default input/output)";
        }
        else if (i == default_output || i == default_input)
        {
            default_io = i == default_output ? " (default output)" : " (default input)";
        }
        
        std::string caption = std::string(device_info->name) + " - " + std::to_string(num_inputs) + " inputs / " + std::to_string(num_outputs) + " outputs" + default_io;
        Log::info(std::to_string(i).c_str(), caption.c_str());
    }
}

//-------------------------------------------------------------------------------------------------

void Audio::start(int bits, int rate, int device_id)
{
    if (Pa_IsStreamActive(m_stream))
    {
        Pa_AbortStream(m_stream);
        Pa_CloseStream(m_stream);
    }

    PaSampleFormat format;

    switch (bits)
    {
        case 16:
        format = paInt16;
        m_frame_size = 2 * 2;
        break;
        case 24:
        format = paInt24;
        m_frame_size = 3 * 2;
        break;
        case 32:
        format = paFloat32;
        m_frame_size = 4 * 2;
        break;
        default:
        throw std::runtime_error("Invalid bit depth");
    }

    PaStreamParameters output_parameters;
    output_parameters.device = device_id == DEFAULT ? Pa_GetDefaultOutputDevice() : device_id;
    output_parameters.channelCount = 2;
    output_parameters.sampleFormat = format;
    output_parameters.suggestedLatency = Pa_GetDeviceInfo(output_parameters.device)->defaultHighOutputLatency;
    output_parameters.hostApiSpecificStreamInfo = NULL;    

    if (Pa_OpenStream(
        &m_stream,
        NULL, //no inputs just yet
        &output_parameters,
        rate,
        1,
        paNoFlag,
        pa_callback,
        this) != paNoError)
    {
        throw std::runtime_error("Could not open stream");
    }

    if (Pa_StartStream(m_stream) != paNoError)
    {
        throw std::runtime_error("Could not start stream");
    }
}

//-------------------------------------------------------------------------------------------------

void Audio::stop()
{
    if (Pa_IsStreamActive(m_stream) && Pa_AbortStream(m_stream) != paNoError)
    {
        throw std::runtime_error("Could not stop stream");
    }
}

//-------------------------------------------------------------------------------------------------
}//pi_kit
