#pragma once

#include <functional>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

template <typename TEventData = int>
struct Event
{
	TEventData m_data;
	std::function<void(TEventData&)> m_send_callback;
};

//-------------------------------------------------------------------------------------------------

template <typename TEventData = int>
class EventSender
{
public:

	using Callback = std::function<void(TEventData&)>;

    virtual std::shared_ptr<Event<TEventData>> on_event_thread_update() // (fast) update from events thread
    {
    	return nullptr;
    }

    void set_events_callback(Callback callback)
    {
    	m_send_callback = callback;
    }

protected:

	std::shared_ptr<Event<TEventData>> createEvent(const TEventData data)
	{
		auto event = std::make_shared<Event<TEventData>>();
		event->m_data = data;
		event->m_send_callback = m_send_callback;
		return event;
	}

private:

	Callback m_send_callback;
};

//-------------------------------------------------------------------------------------------------

template <typename TEventData = int>
class EventThread
{
public:
	
	using EventSenderPtr = std::shared_ptr<EventSender<TEventData>>;
	using EventQueue = std::queue<std::shared_ptr<Event<TEventData>>>;

	void start()
	{
	    m_stop = false;
	    m_thread.reset(new std::thread([&](){
	        while(!m_stop)
	        {
	            for (auto sender: m_senders)
	            {
	                if (auto event = sender->on_event_thread_update())
	                {
	                    std::lock_guard<std::mutex> lock(m_mutex);
	                    m_queue.push(event);
	                }
	            }
	        }
	    }));
	}

	void stop()
	{
	    m_stop = true;
	    if (m_thread)
	    {
	        m_thread->join();
	        m_thread.reset(nullptr);
	    }
	}

	bool is_running()
	{
		return m_thread != nullptr;
	}

	void collect_and_dispatch()
	{
	    while(!m_queue.empty())
	    {
	        if (auto event = m_queue.front())
	        {
	        	if (event->m_send_callback)
	        	{
	            	event->m_send_callback(event->m_data);
	            }
	        }
	        std::lock_guard<std::mutex> lock(m_mutex);
	        m_queue.pop();
	    }	
	}

	void reset()
	{
		stop();
	    while(!m_queue.empty())
	    {
	        m_queue.pop();
	    }
	    m_senders.clear();
	}

	void add_sender(EventSenderPtr sender)
	{
		m_senders.push_back(sender);
	}

	void for_each_sender(std::function<void(EventSenderPtr)> func)
	{
		for(EventSenderPtr sender: m_senders)
		{
			func(sender);
		}
	}

private:

    std::unique_ptr<std::thread> m_thread;
    bool m_stop = false;
    mutable std::mutex m_mutex;

	EventQueue m_queue;
	std::vector<EventSenderPtr> m_senders;
};

//-------------------------------------------------------------------------------------------------
}//pi_kit
