#include "fonts.h"
#include "PixelCanvas16.h"

#include <cstring>

namespace pi_kit {

#define RGB(R,G,B)  (((R >> 3) << 11) | ((G >> 2) << 5) | (B >> 3))

//-------------------------------------------------------------------------------------------------

PixelCanvas16::PixelCanvas16(int width, int height)
: m_width(width)
, m_height(height)
{
	m_buffer_size = m_width * m_height * PIXEL_SIZE;
	m_buffer.reset(new unsigned char[m_buffer_size]);
	reset();
}

//-------------------------------------------------------------------------------------------------

void PixelCanvas16::pixel(int x, int y, float r, float g, float b)
{       
    m_modified = true;

    if (x >= m_width || y >= m_height)
    {
        return;
    }

    const int color = RGB((int)(r * 255), (int)(g * 255), (int)(b * 255));
    
    m_buffer.get()[x * PIXEL_SIZE + y * m_width * PIXEL_SIZE] = color >> 8;
    m_buffer.get()[x * PIXEL_SIZE + y * m_width * PIXEL_SIZE + 1] = color;
}

//-------------------------------------------------------------------------------------------------

void PixelCanvas16::clear()
{       
    m_modified = true;
    std::memset(m_buffer.get(), 0, m_buffer_size);
}

//-------------------------------------------------------------------------------------------------

void PixelCanvas16::reset()
{
	std::memset(m_buffer.get(), 0, m_buffer_size);
	m_options.reset(new TextOptions);
}

//-------------------------------------------------------------------------------------------------

void PixelCanvas16::print_char(unsigned char x, unsigned char y, char ascii)
{
    m_modified = true;

    unsigned char i, j, y0=y;
    char temp;
    unsigned char ch = ascii - ' ';

    for (i = 0; i < m_options->size; i++)
    {
        if (m_options->size == 12)
        {
            temp = m_options->invert ? ~s_font_12_06[ch][i] : s_font_12_06[ch][i];
        }
        else 
        {            
            temp = m_options->invert ? ~s_font_16_08[ch][i] : s_font_16_08[ch][i];
        }

        for (j = 0; j < 8; j++)
        {
            if (temp & 0x80)
            {
                pixel(x, y, m_options->r, m_options->g, m_options->b);
            }
            else
            {
                pixel(x, y, 0, 0, 0);
            }
            temp <<= 1;
            y++;
            if ((y - y0) == m_options->size)
            {
                y = y0;
                x ++;
                break;
            }
        }
    }
}

//-------------------------------------------------------------------------------------------------

void PixelCanvas16::print(std::string str)
{
    m_modified = true;

    unsigned char x = m_options->x;
    unsigned char y = m_options->y;
    const char *string = str.c_str();

    while (*string != '\0') 
    {       
        if (m_options->wrap)
        {
            if (x > (m_width - m_options->size / 2)) 
            {
                x = 0;
                y += m_options->size;
                if (y > (m_height - m_options->size))
                {
                    y = x = 0;
                }
            }
        }
        
        print_char(x, y, *string);
        x += m_options->size / 2;
        string ++;
    }
}

//-------------------------------------------------------------------------------------------------
}//pi_kit
