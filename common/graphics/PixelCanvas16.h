#pragma once

#include <string>
#include <memory>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

class PixelCanvas16
{
public:

	enum TextSize
	{
		SMALL = 12,
		BIG = 16
	};

	struct TextOptions
	{
		int x = 0;
		int y = 0;
		float r = 1;
		float g = 1;
		float b = 1;
		int size = BIG;
		bool invert = false;
		bool wrap = false;
	};

	static const int PIXEL_SIZE = 2;

	PixelCanvas16(int width, int height);

	bool modified() const
	{
		return m_modified;
	}

	void reset_modified()
	{
		m_modified = false;
	}

	unsigned char* get_buffer()
	{
		return m_buffer.get();
	}

	int get_size() const
	{
		return m_buffer_size;
	}

	void set_text_size(int size) {m_options->size = size;}
	void set_text_RGB(float r, float g, float b) {m_options->r = r, m_options->g = g, m_options->b = b;}
	void set_text_pos(int x, int y) {m_options->x = x; m_options->y = y;}
	void set_text_invert(bool invert) {m_options->invert = invert;}
	void set_text_wrap(bool wrap) {m_options->wrap = wrap;}

	void print_char(unsigned char x, unsigned char y, char ascii);
	void print(std::string str);
	void pixel(int x, int y, float r, float g, float b);
	void clear();
	void reset();

private:

	int m_width = 0;
	int m_height = 0;
	int m_buffer_size = 0;

	bool m_modified = false;

	std::unique_ptr<TextOptions> m_options;
	std::unique_ptr<unsigned char> m_buffer;
};

//-------------------------------------------------------------------------------------------------
}//pi_kit
