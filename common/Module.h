#pragma once

namespace pi_kit {

class Module
{
public:

	virtual void reset() {}

	virtual void on_frame() {}
	virtual void on_error() {}
};

}//pi_kit
