#pragma once

#include <common/Module.h>

#include <iostream> //cout
#include <vector>

namespace pi_kit {

//-------------------------------------------------------------------------------------------------

class App
{

public:

    void add_module(std::shared_ptr<Module> module)
    {
        m_modules.push_back(module);
    }

    void update()
    {
        for(auto& module: m_modules)
        {
            module->on_frame();
        }
    }
    
    void for_each_module(std::function<void(std::shared_ptr<Module>)> func)
    {
        for(auto module: m_modules)
        {
            func(module);
        }
    }

private:

    std::vector<std::shared_ptr<Module>> m_modules;
    
};

//-------------------------------------------------------------------------------------------------
}//pi_kit
