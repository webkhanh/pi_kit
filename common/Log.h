#pragma once

#include <string>
#include <iostream>
#include <ctime>
#include <vector>

namespace pi_kit {

class Log 
{
public:

    static constexpr const char* white_thin = "\033[0;37m";
    static constexpr const char* white_bold = "\033[1;37m";
    static constexpr const char* yellow_thin = "\033[0;33m";
    static constexpr const char* yellow_bold = "\033[1;33m";
    static constexpr const char* green_thin = "\033[0;32m";
    static constexpr const char* green_bold = "\033[1;32m";
    static constexpr const char* blue_thin = "\033[0;34m";
    static constexpr const char* blue_bold = "\033[1;34m";
    static constexpr const char* cyan_thin = "\033[0;36m";
    static constexpr const char* cyan_bold = "\033[1;36m";
    static constexpr const char* red_bold = "\033[1;31m";
    static constexpr const char* red_thin = "\033[0;31m";
    static constexpr const char* reset = "\033[0m";

    //-------------------------------------------------------------------------------------------------

    static void app(const char* main, const char* sub = "")
    {
        std::cout << yellow_bold << main << " " << yellow_thin << sub << reset << std::endl;        
    }

    //-------------------------------------------------------------------------------------------------

    static void module(const char* name, const char* desc = "")
    {
        std::cout << yellow_thin << "Module " << yellow_bold << name << reset << " " << desc << std::endl;        
    }

    //-------------------------------------------------------------------------------------------------

    static void error(const char* message)
    {
        std::cout << red_bold << message << reset << std::endl;
    }

    //-------------------------------------------------------------------------------------------------

    static void title(const char* type, const char* caption = "")
    {
        std::cout << std::endl << "📘 " << yellow_thin << type << " " << yellow_bold << caption << reset << std::endl;
    }

    //-------------------------------------------------------------------------------------------------

    static void info(const char* main, const char* sub = "")
    {
        std::cout << "  " << cyan_bold << main << " " << reset << sub << std::endl;
    }

    //-------------------------------------------------------------------------------------------------

    static void date()
    {
        std::time_t now = std::time(nullptr);
        std::cout << yellow_thin << std::ctime(&now) << reset;
    }

    //-------------------------------------------------------------------------------------------------

    static void func(const char* function_name, std::vector<const char*> args, const char* desc)
    {
        std::cout << "  " << cyan_bold << function_name << "(";
        for(int i = 0; i < args.size(); ++i)
        {
            std::cout << reset << args[i] << cyan_bold << ((i == args.size() - 1) ? "" : ", ");
        }
        std::cout << cyan_bold << ") ";
        std::cout << reset << desc << std::endl;
    }

    //-------------------------------------------------------------------------------------------------

    static void GPIO()
    {
        auto is_ground = [&](int pin){
            return pin == 6 || pin == 9 || pin == 14 || pin == 20 || pin == 30 || pin == 34 || pin == 39;
        };

        auto set_pin_color = [&](int pin, bool bold){
            //power
            if (pin == 1 || pin == 2 || pin == 4 || pin == 17)
            {
                std::cout << (bold ? red_bold : red_thin);
            }
            //ground
            else if (is_ground(pin))
            {
                std::cout << (bold ? blue_bold : blue_thin);
            }
            //SPI0
            else if (pin == 19 || pin == 21 || pin == 23 || pin == 24 || pin == 26)
            {
                std::cout << (bold ? green_bold : green_thin);
            }
            //unavailable (I2C EEPROM)
            else if (pin == 27 || pin == 28)
            {
                std::cout << white_thin;
            }
            else
            {
                std::cout << (bold ? white_bold : white_thin);
            }
        };

        auto print_pin_label = [&](int pin){
            if (pin == 1 || pin == 17)
            {
                std::cout << "  +3.3V  ";
            }
            else if (pin == 2 || pin == 4)
            {
                std::cout << "  +5.0V  ";
            }
            else if (is_ground(pin))
            {
                std::cout << "  GND    ";
            }
            else if (pin == 19)
            {
                std::cout << "  MOSI   ";
            }
            else if (pin == 21)
            {
                std::cout << "  MISO   ";
            }
            else if (pin == 23)
            {
                std::cout << "  SCLK   ";
            }
            else if (pin == 24)
            {
                std::cout << "  CE0    ";
            }
            else if (pin == 26)
            {
                std::cout << "  CE1    ";
            }
            else if (pin == 27 || pin == 28)
            {
                std::cout << "  N/A    ";
            }
            else
            {
                std::cout << "         ";
            }
        };

        auto print_pin = [&](int pin){
            std::cout << std::to_string(pin);
            if (pin < 10)
            {
                std::cout << " ";
            }
            std::cout << " ";
        };

        Log::info("");

        for (int row = 0; row < 20; ++row)
        {
            int pin = row * 2 + 1;
            set_pin_color(pin, false);
            print_pin_label(pin);
            set_pin_color(pin, true);
            print_pin(pin);
            std::cout << " ";

            pin = row * 2 + 2;
            set_pin_color(pin, true);
            print_pin(pin);
            set_pin_color(pin, false);
            print_pin_label(pin);
            std::cout << std::endl;
        }

        Log::info("");
        std::cout << white_thin << "  GPIO logic pins:" << std::endl;
        std::cout << white_thin << "  All logic pins are +3.3V (NOT +5V tolerant)" << std::endl;
        std::cout << white_thin << "  #3 and #5 are inputs only" << std::endl;
        std::cout << white_thin << "  #27 & #28 are not available (reserved for I2C EEPROM)" << std::endl;
        std::cout << white_thin << "  #12 & #32 (PWM0) and #33 (PWM1) are PWM-capable via hardware (all others are software-driven)" << std::endl;
        Log::info("");
        std::cout << green_thin << "  SPI:" << std::endl;
        std::cout << green_thin << "  Only SPI0 supported 'out of the box'" << std::endl;
        std::cout << green_thin << "  SCLK is also called SCL" << std::endl;
        std::cout << green_thin << "  Component DIN (data in) to be connected to MOSI (master out slave in)" << std::endl;
        std::cout << green_thin << "  Component DOUT (data out) to be connected to MISO (master in slave out)" << std::endl;
        std::cout << green_thin << "  Component CS (chip select) to be connected to CE0 or CE1" << std::endl;
    }
};

//-------------------------------------------------------------------------------------------------

}//pi_kit
