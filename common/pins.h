#pragma once

namespace pi_kit { 
namespace pins {

//-------------------------------------------------------------------------------------------------

const int NA = -1;

static int physical_to_WiringPi(int pin)
{
	static const int V = NA;
	static const int G = NA;
	static const int table[40] = 
	{
		V, V, 
		8, V, 
		9, G, 
		7, 15, 
		G, 16,
		0, 1, 
		2, G, 
		3, 4, 
		V, 5, 
		12, G,
		13, 6, 
		14, 10, 
		G, 11, 
		30, 31, 
		21, G,
		22, 26, 
		23, G, 
		24, 27, 
		25, 28, 
		G, 29
	};
	return pin > 0 && pin <= 40 ? table[pin-1] : NA;
}

//-------------------------------------------------------------------------------------------------

namespace {
    bool is_one_of(int pin, const std::vector<int> list)
    {
        for (int index: list)
        {
            if (index == pin)
            {
                return true;
            }
        }
        return false;  
    }
}

//-------------------------------------------------------------------------------------------------

static bool is_input_capable(int pin)
{
    return is_one_of(pin, {3, 5, 7, 8, 10, 11, 12, 13, 15, 16, 18, 19, 21, 22, 23, 24, 25, 26, 29, 31, 32, 33, 35, 36, 37, 38, 40});
}

//-------------------------------------------------------------------------------------------------

static bool is_output_capable(int pin)
{
    return is_one_of(pin, {7, 8, 10, 11, 12, 13, 15, 16, 18, 19, 21, 22, 23, 24, 25, 26, 29, 31, 32, 33, 35, 36, 37, 38, 40});
}

//-------------------------------------------------------------------------------------------------

static bool is_hw_pwm_capable(int pin)
{
    return pin == 12 || pin == 32 || pin == 33;
}

//-------------------------------------------------------------------------------------------------
}//pins
}//pi_kit
